//
//  EditATCHeaderTableViewCell.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 26/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class EditATCHeaderTableViewCell: UITableViewCell {

    @IBOutlet var day: UILabel!
    @IBOutlet var addNew: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addNew.tintColor = UIColor.white
        addNew.layer.cornerRadius = 12
        addNew.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addHoursRange(_ sender: Any) {
        print("klik klik")
    }

}
