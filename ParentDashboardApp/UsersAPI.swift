//
//  UsersAPI.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 03/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class UsersAPI {
    
    let baseURL: String = "https://service.block.si/config/"
    
    var defaults = UserDefaults.standard
    
    func getHomeUsersByAdmin(_ email: String, completionHandler: @escaping (([User]) -> Void)) -> Void {
        var users: [User] = []
        
        Alamofire.request(baseURL+"getHomeUsersByAdmin/"+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    var user: User
                    
                    for u in responseJSON["HomeUsers"] {
                        let company = u.1["CompanyId"].string!
                        let adminId = u.1["AdminId"].string!
                        let userId = u.1["UserId"].string!
                        let profile = u.1["Profile"].string!
                        let validationCode = u.1["ValidationCode"].string!
                        let pending = u.1["Pending"].boolValue
                        let firstName = u.1["FirstName"].string!
                        let lastName = u.1["LastName"].string!
                        let password = u.1["AccountPassword"].string!
                        
                        
                        if pending != false {
                            user = User(companyId: company, adminId: adminId, userId: userId, profile: profile, validationCode: validationCode, pending: pending, firstName: firstName, lastName: lastName, password: password)
                            
                            users.append(user)
                        }
                        
                        
                    }
                    
                    completionHandler(users)
                }
                    
                else {
                    completionHandler(users)
                }
                
                
            }
            else {
                completionHandler(users)
            }
            
            
        }
    }

    
    func getCompany(_ email: String, completionHandler: @escaping ((Company?) -> Void)) -> Void {
        var company: Company?
        
        Alamofire.request(baseURL+"getCompany/"+email+"/eeparent", method: .get).responseJSON { response in
            print("ttt")
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                let data = responseJSON["Data"]
                
                if responseJSON["status"] == "true" {
                    let id = data["_id"].stringValue
                    let companyId = data["CompanyId"].stringValue
                    let subscriptionId = data["SubscriptionId"].stringValue
                    let maxLicense = data["maxLicense"].intValue
                    let endDate = data["endDate"].stringValue
                    let bmVersion = data["BmVersion"].stringValue
                    let accStatus = data["AccountStatus"].boolValue
                    let startDate = data["startDate"].stringValue
                    let firstName = data["FirstName"].stringValue
                    let lastName = data["LastName"].stringValue
                    let phone = data["Phone"].stringValue
                    let city = data["City"].stringValue
                    let state = data["State"].stringValue
                    let country = data["Country"].stringValue
                    let organization = data["Organization"].stringValue
                    let timezone = data["Timezone"].stringValue
                    let timezoneLoc = data["TimezoneLoc"].stringValue
                    
                    company = Company(id: id, companyId: companyId, subscriptionId: subscriptionId, maxLicense: maxLicense, endDate: endDate, bmVersion: bmVersion, accStatus: accStatus, startDate: startDate, firstName: firstName, lastName: lastName, phone: phone, city: city, state: state, country: country, organization: organization, timezone: timezone, timezoneLoc: timezoneLoc)
                    
                    
                    completionHandler(company!)
                    
                }
                    
                else {
                    completionHandler(company)
                }
                
                
            }
            else {
                completionHandler(company)
                
            }
            
            
        }
    }
    
    func getStudentsForParentAccount(_ email: String, completionHandler: @escaping (([Student]) -> Void)) -> Void {
        var students: [Student] = []
        
        Alamofire.request(baseURL+"getStudentsForParentAccount/"+email+"/eeparent/true", method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for s in responseJSON["Students"] {
                        let companyId = s.1["CompanyId"].stringValue
                        let parentId = s.1["ParentId"].stringValue
                        let studentId = s.1["StudentId"].stringValue
                        let bmVersion = s.1["BmVersion"].stringValue
                        let profile = s.1["Profile"].stringValue
                        let status = s.1["Status"].boolValue
                        let atProfile = s.1["ATProfile"].stringValue
                        let bwList = s.1["BWList"].stringValue
                        
                        students.append(Student(companyId: companyId, parentId: parentId, studentId: studentId, bmVersion: bmVersion, profile: profile, status: status, atProfile: atProfile, bwList: bwList))
                    }
                    
                    completionHandler(students)
                    
                }
                    
                else {
                    students.append(Student(companyId: "", parentId: "", studentId: "", bmVersion: "", profile: "", status: false, atProfile: "", bwList: ""))
                    completionHandler(students)
                }
                
                
            }
            else {
                completionHandler(students)
            }
            
            
        }
    }
    
    //get only users which have status validated
    func getStudentsForParentAccountForTrends(_ email: String, completionHandler: @escaping (([Student]) -> Void)) -> Void {
        var students: [Student] = []
        
        Alamofire.request(baseURL+"getStudentsForParentAccount/"+email+"/eeparent/true", method: .get).responseJSON { response in
            //print(response.request)  // original URL request
            //print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for s in responseJSON["Students"] {
                        if s.1["Status"].boolValue == true {
                            let companyId = s.1["CompanyId"].stringValue
                            let parentId = s.1["ParentId"].stringValue
                            let studentId = s.1["StudentId"].stringValue
                            let bmVersion = s.1["BmVersion"].stringValue
                            let profile = s.1["Profile"].stringValue
                            let status = s.1["Status"].boolValue
                            let atProfile = s.1["ATProfile"].stringValue
                            let bwList = s.1["BWList"].stringValue
                            
                            students.append(Student(companyId: companyId, parentId: parentId, studentId: studentId, bmVersion: bmVersion, profile: profile, status: status, atProfile: atProfile, bwList: bwList))
                        }
                    }
                    
                    completionHandler(students)
                    
                }
                    
                else {
                    students.append(Student(companyId: "", parentId: "", studentId: "", bmVersion: "", profile: "", status: false, atProfile: "", bwList: ""))
                    completionHandler(students)
                }
                
                
            }
            else {
                completionHandler(students)
            }
            
            
        }
    }
    
    func getSchoolAdminForParentAccount(_ email: String, studentEmail: String, completionHandler: @escaping ((String) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"getSchoolAdminForParentAccount/"+email+"/"+studentEmail, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    
                    completionHandler(responseJSON["CompanyId"].stringValue)
                    
                }
                    
                else {
                    completionHandler("")
                }
                
                
            }
            else {
                completionHandler("")
            }
            
            
        }
    }
    
    func setParentFilter(_ email: String, studentId: String, filterName: String, filterType: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        let parameters: [String: Any] = [
            "companyId": email,
            "studentId": studentId,
            "filterName": filterName,
            "filterType": filterType
            
        ]
        
        Alamofire.request(baseURL+"setParentFilter_v2", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                }
                    
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func setParentFilter_v2(_ email: String, studentId: String? = nil, filterName: String, filterType: String? = nil, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        var parameters: [String: Any] = [:]
        
        parameters = [
            "companyId": email,
            "studentId": studentId,
            "filterName": filterName,
            "filterType": filterType
        ]
        
        Alamofire.request(baseURL+"setParentFilter_v2", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                print(responseJSON)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func removeParentAcc(_ admin: String ,email: String, studentId: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"removeParentAccount/"+admin+"/"+email+"/"+studentId+"/eeparent", method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                }
                    
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func requestParentAccount(_ email: String, studentId: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"requestParentAccount/"+email+"/"+studentId+"/", method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                }
                    
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func blocksiAccLogin(_ email: String, password: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"checkBlocksiAccount/"+email+"/"+password, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                }
                    
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func createBlocksiParentAcc(_ email: String, password: String, firstname: String, lastName: String, phone: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"createBlocksiParentAccount/"+email+"/"+password+"/"+firstname+"/"+lastName+"/"+phone, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                }
                    
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
        }
    }
    
    func requestParentAcc(_ companyId: String, studentEmail: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"requestParentAccount/"+companyId+"/"+studentEmail, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                }
                    
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
        }
    }
    
    func sendWelcomeMsg(_ companyId: String, firstName: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"sendWelcomeEmail/"+companyId+"/"+firstName+"/eeparent", method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                }
                    
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
        }
    }
}
