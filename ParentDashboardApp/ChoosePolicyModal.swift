//
//  ChoosePolicyModal.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 04/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Toaster

class ChoosePolicyModal: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let thePicker = UIPickerView()
    let thePicker2 = UIPickerView()

    @IBOutlet var modalView: UIView!
    @IBOutlet var ATprofileName: UITextField!
    @IBOutlet var bwListName: UITextField!
    
    var atprofiles: [Profile] = []
    var list: [BW] = []
    var selectedProfile: String = ""
    var selectedList: String = ""
    var studentId: String?
    
    var email: String = ""
    var defaults = UserDefaults.standard
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        list.insert(BW(id: "", name: "none"), at: 0)
        
        addKeyboardToolBar()
        addKeyboardToolBar2()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        modalView.layer.cornerRadius = 8
        
        ATprofileName.inputView = thePicker
        ATprofileName.text = selectedProfile
        
        thePicker.delegate = self
        
        bwListName.inputView = thePicker2
        bwListName.text = selectedList
        
        thePicker2.delegate = self
        
        thePicker.backgroundColor = UIColor(red:0.91, green:0.93, blue:0.99, alpha:1.0)
        thePicker2.backgroundColor = UIColor(red:0.91, green:0.93, blue:0.99, alpha:1.0)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

    func donePicker(){
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //add buttons to ATC picker
    func addKeyboardToolBar() {
        var nextButton, cancelButton: UIBarButtonItem?
        var keyboardToolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(thePicker.frame.size.width), height: CGFloat(25)))
        keyboardToolBar.sizeToFit()
        keyboardToolBar.barStyle = .default
        keyboardToolBar.barTintColor = .white
        ATprofileName.inputAccessoryView = keyboardToolBar
        cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelPicker))
        nextButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(self.selectATC))
        keyboardToolBar.items = [cancelButton!,UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton!]
    }
    
    //add buttons to bw list picker
    func addKeyboardToolBar2() {
        var nextButton, cancelButton: UIBarButtonItem?
        var keyboardToolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(thePicker2.frame.size.width), height: CGFloat(25)))
        keyboardToolBar.sizeToFit()
        keyboardToolBar.barStyle = .default
        keyboardToolBar.barTintColor = .white
        bwListName.inputAccessoryView = keyboardToolBar
        cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelPicker))
        nextButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(self.selectBW))
        keyboardToolBar.items = [cancelButton!,UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton!]
    }
    
    func cancelPicker(){
        self.view.endEditing(true)
    }
    
    func selectATC(){
        let selectedValue = atprofiles[thePicker.selectedRow(inComponent: 0)].profileId
        ATprofileName.text = selectedValue
        self.view.endEditing(true)
    }
    
    func selectBW(){
        let selectedValue = list[thePicker2.selectedRow(inComponent: 0)]
        bwListName.text = selectedValue.name
        self.view.endEditing(true)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveToDB(_ sender: Any) {
        
        if atprofiles[thePicker.selectedRow(inComponent: 0)].id == ""{
            UsersAPI().setParentFilter(self.email, studentId: studentId!, filterName: ATprofileName.text!, filterType: "atc", completionHandler: { result in
                if result == true {
                    
                    if self.list[self.thePicker2.selectedRow(inComponent: 0)].id == ""{
                        UsersAPI().setParentFilter(self.email, studentId: self.studentId!, filterName: self.bwListName.text!, filterType: "bwlist", completionHandler: { result in
                            if result == true {
                                ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                                    if result == true {
                                        Toast(text: "Success!", duration: Delay.short).show()
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    else {
                                        Toast(text: "Something went wrong!", duration: Delay.short).show()
                                    }
                                })
                            }
                        })
                    }
                    else {
                        UsersAPI().setParentFilter_v2(self.email, studentId: self.studentId!, filterName: self.bwListName.text!, filterType: "bwlist", completionHandler: { result in
                            if result == true {
                                ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                                    if result == true {
                                        Toast(text: "Success!", duration: Delay.short).show()
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    else {
                                        Toast(text: "Something went wrong!", duration: Delay.short).show()
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
        else {
            UsersAPI().setParentFilter_v2(self.email, studentId: studentId!, filterName: ATprofileName.text!, filterType: "atc", completionHandler: { result in
                if result == true {
                    
                    if self.list[self.thePicker2.selectedRow(inComponent: 0)].id == ""{
                        UsersAPI().setParentFilter(self.email, studentId: self.studentId!, filterName: self.bwListName.text!, filterType: "bwlist", completionHandler: { result in
                            if result == true {
                                ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                                    if result == true {
                                        Toast(text: "Success!", duration: Delay.short).show()
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    else {
                                        Toast(text: "Something went wrong!", duration: Delay.short).show()
                                    }
                                })
                            }
                        })
                    }
                    else {
                        UsersAPI().setParentFilter_v2(self.email, studentId: self.studentId!, filterName: self.bwListName.text!, filterType: "bwlist", completionHandler: { result in
                            if result == true {
                                ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                                    if result == true {
                                        Toast(text: "Success!", duration: Delay.short).show()
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    else {
                                        Toast(text: "Something went wrong!", duration: Delay.short).show()
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case thePicker:
            return atprofiles.count
        case thePicker2:
            return list.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case thePicker:
            return atprofiles[row].profileId
        case thePicker2:
            return list[row].name
        default:
            return ""
        }
    }
    
    /**
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case thePicker:
            ATprofileName.text = atprofiles[row]
        case thePicker2:
            bwListName.text = list[row]
        default:
            break
        }
    }
    
    **/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
