//
//  UsersAPI.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 03/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class TrendsAPI {
    
    let baseURL = "https://service.block.si/config/"
    let barkEndPoint = "https://www.bark.us/api/v1/partners"
    let barkPartnerToken = "L4Umof5mGwHbXSwnrqSPBTrd"
    
    func getHomeUsersByAdmin(_ email: String, domain: String, completionHandler: @escaping (([String], [[String]], String) -> Void)) -> Void {
        var issues: [String] = []
        var categories: [[String]] = []
        var URL: String = ""
        
        Alamofire.request(barkEndPoint+"/analytics?domain="+domain+"&token="+barkPartnerToken+"&email="+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["success"] == true {
                    print(responseJSON)
                    issues.append(responseJSON["issues"]["unreviewed"].stringValue)
                    issues.append(responseJSON["issues"]["total"].stringValue)
                    
                    URL = responseJSON["issues_review_url"].stringValue
                    
                    for i in responseJSON["abuse_types"] {
                        categories.append([i.1["name"].stringValue, i.1["total"].stringValue])
                    }
                    
                    completionHandler(issues, categories, URL)
                }
                    
                else {
                    issues.append("0")
                    issues.append("0")
                    print(issues)
                    completionHandler(issues, categories, URL)
                }
                
                
            }
            else {
                completionHandler(issues, categories, URL)
            }
            
            
        }
    }
}
