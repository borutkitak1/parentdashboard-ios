//
//  ProfileAPI.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ProfileAPI {
    
    let baseURL: String = "https://service.block.si/config/"
    
    //let baseURL: String = "http://localhost:3000/config/"
    
    func getAllProfiles(_ email: String, completionHandler: @escaping (([Profile]) -> Void)) -> Void {
        var profiles: [Profile] = []
        
        Alamofire.request(baseURL+"getAllATProfiles/"+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result.value)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for i in responseJSON["Profiles"] {
                        let companyId = i.1["CompanyId"].stringValue
                        let profileId = i.1["ProfileId"].stringValue
                        let type = i.1["Type"].stringValue
                        let monday = i.1["monday"].stringValue
                        let tuesday = i.1["tuesday"].stringValue
                        let wednesday = i.1["wednesday"].stringValue
                        let thursday = i.1["thursday"].stringValue
                        let friday = i.1["friday"].stringValue
                        let saturday = i.1["saturday"].stringValue
                        let sunday = i.1["sunday"].stringValue
                        
                        profiles.append(Profile(id: "", companyId: companyId, profileId: profileId, type: type, monday: monday, tuesday: tuesday, wednesday: wednesday, thursday: thursday, friday: friday, saturday: saturday, sunday: sunday))
                        
                    }
                    completionHandler(profiles)
                    
                }
                else {
                    profiles.append(Profile(id: "", companyId: "", profileId: "empty", type: "", monday: "", tuesday: "", wednesday: "", thursday: "", friday: "", saturday: "", sunday: ""))
                    completionHandler(profiles)
                }
            }
            else {
                completionHandler(profiles)
            }
            
            
        }
    }
    
    func getAllProfiles_v2(_ email: String, completionHandler: @escaping (([Profile]) -> Void)) -> Void {
        var profiles: [Profile] = []
        
        Alamofire.request(baseURL+"getATC_v2/"+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for i in responseJSON["Profiles"] {
                        let companyId = i.1["CompanyId"].stringValue
                        let id = i.1["_id"].stringValue
                        let name = i.1["Name"].stringValue
                        
                        profiles.append(Profile(id: id, companyId: companyId, profileId: name, type: "General", monday: "x", tuesday: "x", wednesday: "x", thursday: "x", friday: "x", saturday: "x", sunday: "x"))
                        
                    }
                    completionHandler(profiles)
                    
                }
                else {
                    profiles.append(Profile(id: "", companyId: "", profileId: "empty", type: "", monday: "", tuesday: "", wednesday: "", thursday: "", friday: "", saturday: "", sunday: ""))
                    completionHandler(profiles)
                }
            }
            else {
                completionHandler(profiles)
            }
            
            
        }
    }
    
    func removeATprofile(_ email: String, list: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"removeATProfile/"+email+"/"+list, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func getProfileByCategory(_ email: String, profileName: String, category: String, completionHandler: @escaping ((Profile) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"getATProfileByCompanyId/"+email+"/"+profileName+"/"+category, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                        let companyId = responseJSON["CompanyId"].stringValue
                        let profileId = responseJSON["ProfileId"].stringValue
                        let monday = responseJSON["monday"].stringValue
                        let tuesday = responseJSON["tuesday"].stringValue
                        let wednesday = responseJSON["wednesday"].stringValue
                        let thursday = responseJSON["thursday"].stringValue
                        let friday = responseJSON["friday"].stringValue
                        let saturday = responseJSON["saturday"].stringValue
                        let sunday = responseJSON["sunday"].stringValue
                    
                        
                    print(responseJSON)
                    completionHandler(Profile(id: "", companyId: companyId, profileId: profileId, type: category, monday: monday, tuesday: tuesday, wednesday: wednesday, thursday: thursday, friday: friday, saturday: saturday, sunday: sunday))
                    
                }
                else {
                    print("hek")
                    completionHandler(Profile(id: "", companyId: "", profileId: "empty", type: "", monday: "x", tuesday: "x", wednesday: "x", thursday: "x", friday: "x", saturday: "x", sunday: "x"))
                }
            }
            else {
                print("hek2")
                completionHandler(Profile(id: "", companyId: "", profileId: "", type: "", monday: "x", tuesday: "x", wednesday: "x", thursday: "x", friday: "x", saturday: "x", sunday: "x"))
            }
            
            
        }
    }
    
    func setATprofile(_ email: String, acName: String, type: String, data:String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"setATProfile/"+email+"/"+acName+"/"+type+"/"+data, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func getAllProfilesNames(_ email: String, completionHandler: @escaping (([String]) -> Void)) -> Void {
        var profiles: [String] = []
        
        Alamofire.request(baseURL+"getAllATProfiles/"+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for i in responseJSON["Profiles"] {
                        let companyId = i.1["CompanyId"].stringValue
                        let profileId = i.1["ProfileId"].stringValue
                        let type = i.1["Type"].stringValue
                        let monday = i.1["monday"].stringValue
                        let tuesday = i.1["tuesday"].stringValue
                        let wednesday = i.1["wednesday"].stringValue
                        let thursday = i.1["thursday"].stringValue
                        let friday = i.1["friday"].stringValue
                        let saturday = i.1["saturday"].stringValue
                        let sunday = i.1["sunday"].stringValue
                        
                        profiles.append(profileId)
                        
                    }
                    profiles.insert("none", at: 0)
                    completionHandler(profiles)
                    
                }
                else {
                    completionHandler(profiles)
                }
            }
            else {
                completionHandler(profiles)
            }
            
            
        }
    }
    
    
    func getATC_v2(_ email: String, id: String, category:String, completionHandler: @escaping ((Profile) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"getATC_v2/"+email+"/"+id, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    let companyId = responseJSON["CompanyId"].stringValue
                    let profileId = responseJSON["_id"].stringValue
                    
                    //print("monday ",responseJSON["Profile"][category]["Monday"].stringValue)
                    
                    
                        
                        let monday = responseJSON["Profile"][category]["Monday"].stringValue
                        let tuesday = responseJSON["Profile"][category]["Tuesday"].stringValue
                        let wednesday = responseJSON["Profile"][category]["Wednesday"].stringValue
                        let thursday = responseJSON["Profile"][category]["Thursday"].stringValue
                        let friday = responseJSON["Profile"][category]["Friday"].stringValue
                        let saturday = responseJSON["Profile"][category]["Saturday"].stringValue
                        let sunday = responseJSON["Profile"][category]["Sunday"].stringValue
                        
                        
                        print(responseJSON)
                    completionHandler(Profile(id: profileId, companyId: companyId, profileId: "", type: category, monday: monday, tuesday: tuesday, wednesday: wednesday, thursday: thursday, friday: friday, saturday: saturday, sunday: sunday))
                    
                    
                    
                    
                }
                else {
                    print("hek")
                    completionHandler(Profile(id: "", companyId: "", profileId: "empty", type: "", monday: "x", tuesday: "x", wednesday: "x", thursday: "x", friday: "x", saturday: "x", sunday: "x"))
                }
            }
            else {
                print("hek2")
                completionHandler(Profile(id: "", companyId: "", profileId: "", type: "", monday: "x", tuesday: "x", wednesday: "x", thursday: "x", friday: "x", saturday: "x", sunday: "x"))
            }
            
            
        }
    }

    
    func accessTimeControl_v2(_ email: String, id: String? = nil, op: String, name: String? = nil, json: [String:Any]? = nil, type: String? = nil, completionHandler: @escaping ((Bool, String?) -> Void)) -> Void {
        
        var parameters: [String: Any] = [:]
        
        switch op {
        case "remove":
            parameters = [
                "companyId": email,
                "op": op,
                "filterId": id
            ]
        case "create":
            parameters = [
                "companyId": email,
                "op": op,
                "name": name,
                "extraCategories": true
            ]
        case "update_mobile":
            parameters = json!
        default:
            break
        }
        
        print(parameters)
        
        Alamofire.request(baseURL+"accessTimeControl_v2", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                print(responseJSON)
                
                if responseJSON["status"] == "true" {
                    
                    if responseJSON["id"].exists() {
                        print("iddd "+responseJSON["id"].stringValue)
                        completionHandler(true, responseJSON["id"].stringValue)
                    }
                    else {
                        completionHandler(true, nil)
                    }
                }
                else {
                    completionHandler(false, nil)
                }
            }
            else {
                completionHandler(false, nil)
            }
            
            
        }
    }
}
