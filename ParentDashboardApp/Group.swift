//
//  Group.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 04/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

class Group {

    var companyId: String
    var groupId: String
    var filterId: String
    var ytFilterId: String
    var atProfileId: String
    var listId: String
    var regexId: String
    var ytChannelListId: String
    var ytKeywordsId: String
    var safeSearch: Bool
    var blockURL: String
    var appFilterId: String
    
    init(companyId: String, groupId: String, filterId: String, ytFilterId: String, atProfileId: String, listId: String, regexId: String, ytChannelListId: String, ytKeywordsId: String, safeSearch: Bool, blockURL: String, appFilterId: String){
        self.companyId = companyId
        self.groupId = groupId
        self.filterId = filterId
        self.ytFilterId = ytFilterId
        self.atProfileId = atProfileId
        self.listId = listId
        self.regexId = regexId
        self.ytChannelListId = ytChannelListId
        self.ytKeywordsId = ytKeywordsId
        self.safeSearch = safeSearch
        self.blockURL = blockURL
        self.appFilterId = appFilterId
        
    }


}
