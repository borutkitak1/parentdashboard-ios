//
//  BWlistAPI.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class BWlistAPI {
    
    let baseURL: String = "https://service.block.si/config/"
    
    //let baseURL: String = "http://localhost:3000/config/"
    
    func getBWlists(_ email: String, completionHandler: @escaping (([BW]) -> Void)) -> Void {
        var lists: [BW] = []
        
        Alamofire.request(baseURL+"getLists/"+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for i in responseJSON["Lists"] {
                        lists.append(BW(id: "", name: i.1.stringValue))
                    }
                    completionHandler(lists)
                    
                }
                else {
                    lists.append(BW(id: "", name: ""))
                    completionHandler(lists)
                }
            }
            else {
                completionHandler(lists)
            }
            
            
        }
    }
    
    func getBWlists_v2(_ email: String, completionHandler: @escaping (([BW]) -> Void)) -> Void {
        var lists: [BW] = []
        
        Alamofire.request(baseURL+"getBWLists_v2/"+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for i in responseJSON["BWLists"] {
                        
                        let id = i.1["_id"].stringValue
                        let name = i.1["Name"].stringValue
                        
                        lists.append(BW(id: id, name: name))
                    }
                    completionHandler(lists)
                    
                }
                else {
                    lists.append(BW(id: "", name: ""))
                    completionHandler(lists)
                }
            }
            else {
                completionHandler(lists)
            }
            
            
        }
    }
    
    func getURLs_v2(_ email: String, listName: String, completionHandler: @escaping (([BWlist]) -> Void)) -> Void {
        var lists: [BWlist] = []
        
        Alamofire.request(baseURL+"getBWLists_v2/"+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for i in responseJSON["BWLists"] {
                        
                        if i.1["Name"].stringValue == listName {
                            for j in i.1["Urls"] {
                                var act: Bool
                                
                                let action = j.1["Action"].stringValue
                                
                                if action == "0" {
                                    act = true
                                }
                                else{
                                    act = false
                                }
                                
                                lists.append(BWlist(list: listName, url: j.1["Url"].stringValue, action: act))
                            }
                        }
                        
                    }
                    completionHandler(lists)
                    
                }
                else {
                    lists.append(BWlist(list: listName, url: "", action: false))
                    completionHandler(lists)
                }
            }
            else {
                completionHandler(lists)
            }
            
            
        }
    }
    
    func getURLsForList(_ email: String, name: String, completionHandler: @escaping (([BWlist]) -> Void)) -> Void {
        var lists: [BWlist] = []
        
        Alamofire.request(baseURL+"getUrlsForList/"+email+"/"+name, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    for i in responseJSON["urls"] {
                        var act: Bool
                        
                        let listName = i.1["List"].stringValue
                        let url = i.1["Url"].stringValue
                        let action = i.1["Action"].stringValue
                        
                        if action == "0" {
                            act = true
                        }
                        else{
                            act = false
                        }
                        
                        lists.append(BWlist(list: listName, url: url, action: act))
                    }
                    completionHandler(lists)
                    
                }
                else {
                    completionHandler(lists)
                }
            }
            else {
                completionHandler(lists)
            }
            
            
        }
    }
    
    func addURL(_ email: String, list: String, url: String, action: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"addToList/"+email+"/"+list+"/"+url+"/"+action, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func addNewURL(_ email: String, list: String, url: String, action: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"addToList/"+email+"/"+list+"/"+url+"/"+action+"/true", method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func removeFromList(_ email: String, list: String, url: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"removeFromList/"+email+"/"+list+"/"+url, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func removeList(_ email: String, list: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"removeList/"+email+"/"+list, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func BWList_v2(_ email: String, id: String? = nil, op: String, urls:[String]? = nil, url: String? = nil, name: String? = nil, action: String? = nil, completionHandler: @escaping ((Bool, String?) -> Void)) -> Void {
        
        var parameters: [String: Any] = [:]
        
        switch op {
        case "removeFilter":
            parameters = [
                "companyId": email,
                "op": op,
                "filterId": id
            ]
        case "create":
            parameters = [
                "companyId": email,
                "op": op,
                "name": name,
                "urls": urls
            ]
        case "add":
            parameters = [
                "companyId": email,
                "op": op,
                "url": url,
                "action": action,
                "bwListId": id
            ]
        case "update":
            parameters = [
                "companyId": email,
                "op": op,
                "url": url,
                "action": action,
                "bwListId": id
            ]
        case "removeUrl":
            parameters = [
                "companyId": email,
                "op": op,
                "url": url,
                "bwListId": id
            ]
        default:
            break
        }
        
        print(parameters)
        
        Alamofire.request(baseURL+"bwList_v2", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                print(responseJSON)
                
                if responseJSON["status"] == "true" {
                    
                    if responseJSON["id"].exists() {
                       completionHandler(true, responseJSON["id"].stringValue)
                    }
                    else {
                        completionHandler(true, nil)
                    }
                }
                else {
                    completionHandler(false, nil)
                }
            }
            else {
                completionHandler(false, nil)
            }
            
            
        }
    }
}
