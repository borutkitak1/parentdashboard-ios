//
//  UpdateEmailModal.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 04/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class UpdateEmailModal: UIViewController {
    
    
    @IBOutlet var modalView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        modalView.layer.cornerRadius = 8

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateEmail(_ sender: Any) {
        
    }
    
    
}
