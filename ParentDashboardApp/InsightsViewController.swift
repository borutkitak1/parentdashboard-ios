//
//  InsightsViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 30/08/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Font_Awesome_Swift
import Toaster
import SwiftDate

class InsightsViewController: UIViewController, UIWebViewDelegate, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet var selectUserBtn: UIButton!
    @IBOutlet var caretDown: UIImageView!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var chooseStudent: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet var pickFromDate: UITextField!
    @IBOutlet var pickToDate: UITextField!
    
    var datePicker = UIDatePicker()
    var datePickerEnd = UIDatePicker()
    let formatter = DateFormatter()
    
    let defaults = UserDefaults.standard
    var email = ""
    var timezone = ""
    var allowed = [URLs]()
    var blocked = [URLs]()
    var students = [Student]()
    var companyId = ""
    var selectedUser = ""
    var selectedDateFrom = ""
    var selectedDateTo = ""
    
    var picker = UIPickerView()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        loadingIndicator.hidesWhenStopped = true
        
        segmentControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.gray], for: UIControlState.normal)
        
        segmentControl.layer.borderColor = UIColor.gray.cgColor
        segmentControl.layer.borderWidth = 1.0
        segmentControl.layer.cornerRadius = 4.0
        segmentControl.clipsToBounds = true
        
        segmentControl.subviews[0].tintColor = UIColor(red:0.95, green:0.26, blue:0.21, alpha:1.0)
        
        //prepare picker to open on choose student text
        chooseStudent.inputView = picker
        picker.delegate = self
        
        picker.backgroundColor = UIColor(red:0.91, green:0.93, blue:0.99, alpha:1.0)
        
        caretDown.setFAIconWithName(icon: .FACaretDown, textColor: UIColor(red:0.00, green:0.48, blue:1.00, alpha:1.0))
        
        selectUserBtn.backgroundColor = .white
        selectUserBtn.layer.cornerRadius = 5
        
        if selectUserBtn.currentTitle == "Choose student" {
            selectUserBtn.isEnabled = false
        }
        
        addKeyboardToolBar()
        addKeyboardToolBar2()
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        let date = Date()
        let dateEnd = Date()-1.day
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        let startDate = formatter.string(from: date)
        let endDate = formatter.string(from: dateEnd)
        
        selectedDateFrom = endDate
        selectedDateTo = startDate
        
        pickFromDate.text = endDate
        pickToDate.text = startDate
        
        pickFromDate.inputView = datePicker
        pickToDate.inputView = datePickerEnd
        
        datePicker.datePickerMode = .date
        datePickerEnd.datePickerMode = .date
        
        datePicker.date = dateEnd
        
        datePicker.backgroundColor = UIColor(red:0.91, green:0.93, blue:0.99, alpha:1.0)
        datePickerEnd.backgroundColor = UIColor(red:0.91, green:0.93, blue:0.99, alpha:1.0)
        
        
        loadingIndicator.startAnimating()
        
        UsersAPI().getStudentsForParentAccount(email, completionHandler: { result in
            if result.count != 0 {
                if result[0].companyId != "" {
                    self.selectUserBtn.isEnabled = true
                    self.students = result
                    self.selectUserBtn.setTitle(result[0].studentId, for: .normal)
                    self.companyId = result[0].companyId
                    self.selectedUser = result[0].studentId
                    
                    self.getAnalyticsForUser()
                }
                else{
                    Toast(text: "Could not load data!", duration: Delay.short).show()
                    self.loadingIndicator.stopAnimating()
                }
            }
            else {
                Toast(text: "Could not load data!", duration: Delay.short).show()
                self.loadingIndicator.stopAnimating()
            }
        })

        // Do any additional setup after loading the view.
    }
    
    func addKeyboardToolBar() {
        var nextButton, cancelButton: UIBarButtonItem?
        let keyboardToolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(datePicker.frame.size.width), height: CGFloat(25)))
        keyboardToolBar.sizeToFit()
        keyboardToolBar.barStyle = .default
        keyboardToolBar.barTintColor = .white
        pickFromDate.inputAccessoryView = keyboardToolBar
        cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelPicker))
        nextButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.done2))
        keyboardToolBar.items = [cancelButton!,UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton!]
    }
    
    func addKeyboardToolBar2() {
        var nextButton, cancelButton: UIBarButtonItem?
        let keyboardToolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(datePickerEnd.frame.size.width), height: CGFloat(25)))
        keyboardToolBar.sizeToFit()
        keyboardToolBar.barStyle = .default
        keyboardToolBar.barTintColor = .white
        pickToDate.inputAccessoryView = keyboardToolBar
        cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelPicker))
        nextButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.done))
        keyboardToolBar.items = [cancelButton!,UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton!]
    }
    
    func cancelPicker(){
        self.view.endEditing(true)
    }
    
    func back(){
        pickFromDate.becomeFirstResponder()
    }
    
    func done2(){
        let d = formatter.string(from: datePicker.date)
        pickFromDate.text = d
        selectedDateFrom = d
        
        if(datePicker.date <= datePickerEnd.date){
            getAnalyticsForUser()
            
            self.view.endEditing(true)
        }
        else {
            Toast(text: "The To date must be greater than the From date!", duration: Delay.short).show()
        }
        
    }
    
    func done(){
        let d = formatter.string(from: datePickerEnd.date)
        pickToDate.text = d
        selectedDateTo = d
        
        if(datePicker.date <= datePickerEnd.date){
            getAnalyticsForUser()
            
            self.view.endEditing(true)
        }
        else {
            Toast(text: "The To date must be greater than the From date!", duration: Delay.short).show()
        }
        
    }
    
    @IBAction func openPicker(_ sender: Any) {
        chooseStudent.becomeFirstResponder()
    }
    
    
    func getAnalyticsForUser(){
        loadingIndicator.startAnimating()
        
        AnalyticsAPI().getURLs(companyId, startDate: selectedDateFrom, endDate: selectedDateTo, studentEmail: selectedUser, timezone: "none", completionHandler: { result, result2 in
            self.allowed = result
            self.blocked = result2
            
            self.loadingIndicator.stopAnimating()
            
            self.tableView.reloadData()
        })
    }
    
    @IBAction func onSegmentChange(_ sender: UISegmentedControl) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            sender.subviews[1].tintColor = UIColor(red:0.30, green:0.69, blue:0.31, alpha:1.0)
        case 1:
            sender.subviews[0].tintColor = UIColor(red:0.95, green:0.26, blue:0.21, alpha:1.0)
        default:
            break
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentControl.selectedSegmentIndex == 0 {
            return allowed.count
        }
        else {
            return blocked.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "insightsCell") as! InsightsURLsTableViewCell
        switch segmentControl.selectedSegmentIndex {
        case 0:
            cell.url.text = allowed[indexPath.row].url
            cell.hits.text = allowed[indexPath.row].hits
            cell.hits.textColor = UIColor(red:0.30, green:0.69, blue:0.31, alpha:1.0)
            return cell
        case 1:
            cell.url.text = blocked[indexPath.row].url
            cell.hits.text = blocked[indexPath.row].hits
            cell.hits.textColor = UIColor(red:0.95, green:0.26, blue:0.21, alpha:1.0)
            return cell
        default:
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "insightsHeader") as! InsightsHeaderTableViewCell
        headerCell.contentView.backgroundColor = .white
        
        headerCell.hits.text = "Hits"
        headerCell.url.text = "URL"
        
        return headerCell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return students.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return students[row].studentId
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectUserBtn.setTitle(students[row].studentId, for: .normal)
        selectedUser = students[row].studentId
        companyId = students[row].companyId
        self.view.endEditing(true)
        getAnalyticsForUser()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
