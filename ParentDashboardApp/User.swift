//
//  User.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 03/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

class User{
    
    var companyId: String
    var adminId: String
    var userId: String
    var profile: String
    var validationCode: String
    var pending: Bool
    var firstName: String
    var lastName: String
    var password: String
    
    
    init(companyId: String, adminId: String, userId: String, profile: String, validationCode: String, pending: Bool, firstName: String, lastName: String, password: String){
        self.companyId = companyId
        self.adminId = adminId
        self.userId = userId
        self.profile = profile
        self.validationCode = validationCode
        self.pending = pending
        self.firstName = firstName
        self.lastName = lastName
        self.password = password
    }
}
