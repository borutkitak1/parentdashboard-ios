//
//  BarkViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 29/09/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Alamofire

class BarkViewController: UIViewController {

    
    @IBOutlet weak var modalWindow: UIView!
    @IBOutlet weak var barkImg: UIImageView!
    
    let defaults = UserDefaults.standard
    var email: String = ""
    
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        modalWindow.layer.cornerRadius = 8
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        UsersAPI().getCompany(email, completionHandler: { result in
            if result != nil {
                self.url = "https://www.bark.us/api/v1/partners/signup?token=L4Umof5mGwHbXSwnrqSPBTrd&email=\(self.email)&pk="+(result?.id)!
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func barkSignUp(_ sender: Any) {
        print("url je")
        print(url)
    }
    
    
    @IBAction func downloadBark(_ sender: Any) {
        let url = URL(string: "itms-apps://itunes.apple.com/app/bark-monitor-detect-alert/id1164495253")
        print(url!)
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
        }
    }
    
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signUpBark" {
            let des = segue.destination as! BarkSignUpViewController
            des.url = self.url
        }
        
    }
 

}
