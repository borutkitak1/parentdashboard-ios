//
//  TrendsViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 12/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Charts
import Toaster

class TrendsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var pieChartView: PieChartView!
    @IBOutlet var reviewBtn: UIButton!
    
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var chooseUser: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var barkBtn: UIButton!
    
    var picker = UIPickerView()
    var users: [Student] = []
    
    var email: String = ""
    var defaults = UserDefaults.standard
    
    var trends = ["0", "0"]
    var trendsURL = ""
    
    /*
     let url = URL(string: "itms-apps://itunes.apple.com/app/bark-monitor-detect-alert/id1164495253")
     print(url!)
     if #available(iOS 10.0, *) {
     UIApplication.shared.open(url!, options: [:], completionHandler: nil)
     } else {
     UIApplication.shared.openURL(url!)
     }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(TrendsViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TrendsViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        chooseUser.layer.borderWidth = 1
        chooseUser.layer.borderColor = UIColor.lightGray.cgColor
        chooseUser.layer.cornerRadius = 5
        chooseUser.backgroundColor = UIColor.white
        
        self.view.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.95, alpha:1.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        self.chooseUser.isEnabled = false
        
        if UIScreen.main.nativeBounds.height == 1136{
            barkBtn.setTitle("Trends are based on Bark technologies.", for: .normal)
        }
        else{
            barkBtn.setTitle("Trends are based on Bark technologies. Learn more.", for: .normal)
        }
        
        UsersAPI().getStudentsForParentAccountForTrends(email, completionHandler: { result in
            print("ttttttt "+result[0].companyId)
            if result.count != 0 {
                if result[0].companyId != "" {
                    print("tleeee "+result[0].studentId)
                    self.users = result
                    self.chooseUser.text! = result[0].studentId
                    self.getUserTrends(studentId: result[0].studentId)
                    self.chooseUser.isEnabled = true
                    self.reviewBtn.isEnabled = true
                }
                else{
                    self.loadingIndicator.stopAnimating()
                    Toast(text: "Could not load data!", duration: Delay.short).show()
                    self.chooseUser.text = "You do not have any users."
                    self.chooseUser.isEnabled = false
                    self.reviewBtn.backgroundColor = .gray
                }
            }
            else {
                self.loadingIndicator.stopAnimating()
                Toast(text: "Could not load data!", duration: Delay.short).show()
                self.chooseUser.text = "You do not have any users."
                self.chooseUser.isEnabled = false
                self.reviewBtn.isEnabled = false
                self.reviewBtn.backgroundColor = .gray
            }
        })
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
        
        chooseUser.inputView = picker
        picker.delegate = self
        
        picker.backgroundColor = UIColor(red:0.91, green:0.93, blue:0.99, alpha:1.0)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: NSNotification){
        tableView.isUserInteractionEnabled = false
    }
    
    func keyboardWillHide(_ notification: NSNotification){
        tableView.isUserInteractionEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getUserTrends(studentId: String){
        
        if !loadingIndicator.isAnimating {
            loadingIndicator.startAnimating()
        }
        
        var domain = studentId.components(separatedBy: "@")
        
        if domain.count == 2 {
            TrendsAPI().getHomeUsersByAdmin(studentId, domain: domain[1], completionHandler: { result, result2, url in
                print("rezzz \(result) -- \(result2) -- \(url)")
                if result.count != 0 && result2.count != 0 {
                    self.trends = result
                    
                    if url == "" {
                        self.reviewBtn.isUserInteractionEnabled = false
                        self.reviewBtn.backgroundColor = .gray
                    }
                    else {
                        self.trendsURL = url
                        self.reviewBtn.isUserInteractionEnabled = true
                        self.reviewBtn.backgroundColor = UIColor(red:0.27, green:0.47, blue:0.95, alpha:1.0)
                    }
                    
                    self.fillChartWithData(data: result2, total: result[1])
                    self.tableView.reloadData()
                    self.loadingIndicator.stopAnimating()
                }
                else {
                    self.loadingIndicator.stopAnimating()
                    
                    self.reviewBtn.isUserInteractionEnabled = false
                    self.reviewBtn.backgroundColor = .gray
                    Toast(text: "Could not load data!", duration: Delay.short).show()
                }
            })
        }
        else {
            self.loadingIndicator.stopAnimating()
            Toast(text: "Data is not available!", duration: Delay.short).show()
        }
    }
    
    func fillChartWithData(data: [[String]], total: String){
        
        var dataEntries: [PieChartDataEntry] = []
        for i in 0..<data.count {
            //let dataEntry = PieChartDataEntry(value: Double(data[i][1])!, label: data[i][0], data: data[i][0] as AnyObject)
            let dataEntry = PieChartDataEntry()
            //print("\(Double(data[i][1])) ---- \(Double(total)!)")
            dataEntry.y = Double(data[i][1])!
            dataEntry.label = data[i][0]
            dataEntries.append(dataEntry)
        }
        let chartDataSet = PieChartDataSet(values: dataEntries, label: "Abuse types")
        let chartData = PieChartData(dataSet: chartDataSet)
        
        /**
         procenti na grafu
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        chartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        **/
        
        pieChartView.data = chartData
        
        var colors: [UIColor] = []
        
        for _ in 0..<data.count {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        
        chartDataSet.colors = colors
        
        pieChartView.noDataText = "No data available"
        // user interaction
        pieChartView.isUserInteractionEnabled = true
        
        let d = Description()
        d.text = ""
        pieChartView.chartDescription = d
        
        pieChartView.legend.enabled = false
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return users.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return users[row].studentId
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        chooseUser.text = users[row].studentId
        self.view.endEditing(true)
        
        self.getUserTrends(studentId: users[row].studentId)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "trendsCell", for: indexPath) as! TrendsTableViewCell
        
        cell.unreviewed_issues.text = trends[0]
        cell.issues_nr.text = trends[1]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "trendsReview", sender: nil)
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "trendsReview" {
            let des = segue.destination as! TrendsReviewViewController
            des.url = trendsURL
        }
    }


}
