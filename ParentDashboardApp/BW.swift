//
//  BW.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 07/11/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation

class BW {
    
    var id: String
    var name: String
    
    init(id: String, name: String){
        self.id = id
        self.name = name
    }

}
