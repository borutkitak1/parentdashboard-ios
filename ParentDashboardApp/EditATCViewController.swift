//
//  EditATCViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 12/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Toaster

class EditATCViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var email: String = ""
    var defaults = UserDefaults.standard
    var profileName: String = ""
    
    var generalTable: [[String]] = []
    var socialTable: [[String]] = []
    var fbTable: [[String]] = []
    var ytTable: [[String]] = []
    var streamingTable: [[String]] = []
    var gamingTable: [[String]] = []
    
    var mondayTable: [String] = []
    var tuesdayTable: [String] = []
    var wednesdayTable: [String] = []
    var thursdayTable: [String] = []
    var fridayTable: [String] = []
    var saturdayTable: [String] = []
    var sundayTable: [String] = []
    
    var daysSection = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    var times: [String] = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"]
    var categories = ["General","Facebook","Youtube","Streaming","Gaming","Social Media"]
    
    var newATCProfile = false
    
    var newListId: String?
    var version: Int = 2
    
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var selectCategoryBtn: UIButton!
    @IBOutlet var caretDownImg: UIImageView!
    @IBOutlet var chooseCategory: UITextField!
    @IBOutlet var timePicker: UITextField!
    @IBOutlet var dayPicker: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    
    var pickDay = UIPickerView()
    var pickTime = UIPickerView()
    var pickCategory = UIPickerView()
    
    
    var from, to: Int?
    var selectedDay: String?
    var selectedCategory = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //self.view.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.95, alpha:1.0)
        navBar.topItem?.title = "Edit \(profileName)"
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        caretDownImg.setFAIconWithName(icon: .FACaretDown, textColor: UIColor(red:0.00, green:0.48, blue:1.00, alpha:1.0))
        
        selectCategoryBtn.backgroundColor = .white
        selectCategoryBtn.setTitle(categories[selectedCategory], for: .normal)
        selectCategoryBtn.layer.cornerRadius = 5
        
        if version == 1 {
            getGeneralProfile()
        }
        else {
            getGeneralProfile_v2()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
        
        dayPicker.inputView = pickDay
        pickDay.delegate = self
        
        timePicker.inputView = pickTime
        pickTime.delegate = self
        
        chooseCategory.inputView = pickCategory
        pickCategory.delegate = self
        
        pickTime.backgroundColor = UIColor(red:0.91, green:0.93, blue:0.99, alpha:1.0)
        pickCategory.backgroundColor = UIColor(red:0.91, green:0.93, blue:0.99, alpha:1.0)
        
        addKeyboardToolBar()
        


        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addKeyboardToolBar() {
        var nextButton, cancelButton: UIBarButtonItem?
        var keyboardToolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(pickTime.frame.size.width), height: CGFloat(25)))
        keyboardToolBar.sizeToFit()
        keyboardToolBar.barStyle = .default
        keyboardToolBar.barTintColor = .white
        timePicker.inputAccessoryView = keyboardToolBar
        cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelPicker))
        nextButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(self.saveTime))
        keyboardToolBar.items = [cancelButton!,UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton!]
    }
    
    func cancelPicker(){
        self.view.endEditing(true)
    }
    
    @IBAction func openCategoriesPicker(_ sender: Any) {
        chooseCategory.becomeFirstResponder()
    }
    
    
    func bubbleSort<T: Comparable> (array: [T]) -> [T] {
        var swapped = false
        var sortedArray = array
        
        repeat {
            swapped = false
            for i in 0...array.count - 2 {
                if sortedArray[i] > sortedArray[i + 1] {
                    swap(&sortedArray[i], &sortedArray[i + 1])
                    swapped = true
                }
            }
        } while swapped
        return sortedArray
    }
    
    func getGeneralProfile_v2(){
        ProfileAPI().getATC_v2(email, id: newListId!, category: "General", completionHandler: { result in
            self.generalTable.append(["Monday", result.monday])
            self.generalTable.append(["Tuesday", result.tuesday])
            self.generalTable.append(["Wednesday", result.wednesday])
            self.generalTable.append(["Thursday", result.thursday])
            self.generalTable.append(["Friday", result.friday])
            self.generalTable.append(["Saturday", result.saturday])
            self.generalTable.append(["Sunday", result.sunday])
            self.splitTimeTable(table: self.generalTable)
        })
    }
    
    func getSocialMediaProfile_v2(){
        ProfileAPI().getATC_v2(email, id: newListId!, category: "SocialMedia", completionHandler: { result in
            self.socialTable.append(["Monday", result.monday])
            self.socialTable.append(["Tuesday", result.tuesday])
            self.socialTable.append(["Wednesday", result.wednesday])
            self.socialTable.append(["Thursday", result.thursday])
            self.socialTable.append(["Friday", result.friday])
            self.socialTable.append(["Saturday", result.saturday])
            self.socialTable.append(["Sunday", result.sunday])
            self.splitTimeTable(table: self.socialTable)
        })
    }
    
    func getGamingProfile_v2(){
        ProfileAPI().getATC_v2(email, id: newListId!, category: "Gaming", completionHandler: { result in
            self.gamingTable.append(["Monday", result.monday])
            self.gamingTable.append(["Tuesday", result.tuesday])
            self.gamingTable.append(["Wednesday", result.wednesday])
            self.gamingTable.append(["Thursday", result.thursday])
            self.gamingTable.append(["Friday", result.friday])
            self.gamingTable.append(["Saturday", result.saturday])
            self.gamingTable.append(["Sunday", result.sunday])
            self.splitTimeTable(table: self.gamingTable)
        })
    }
    
    func getFacebookProfile_v2(){
        ProfileAPI().getATC_v2(email, id: newListId!, category: "Facebook", completionHandler: { result in
            self.fbTable.append(["Monday", result.monday])
            self.fbTable.append(["Tuesday", result.tuesday])
            self.fbTable.append(["Wednesday", result.wednesday])
            self.fbTable.append(["Thursday", result.thursday])
            self.fbTable.append(["Friday", result.friday])
            self.fbTable.append(["Saturday", result.saturday])
            self.fbTable.append(["Sunday", result.sunday])
            self.splitTimeTable(table: self.fbTable)
        })
    }
    
    func getYoutubeProfile_v2(){
        ProfileAPI().getATC_v2(email, id: newListId!, category: "Youtube", completionHandler: { result in
            self.ytTable.append(["Monday", result.monday])
            self.ytTable.append(["Tuesday", result.tuesday])
            self.ytTable.append(["Wednesday", result.wednesday])
            self.ytTable.append(["Thursday", result.thursday])
            self.ytTable.append(["Friday", result.friday])
            self.ytTable.append(["Saturday", result.saturday])
            self.ytTable.append(["Sunday", result.sunday])
            self.splitTimeTable(table: self.ytTable)
        })
    }
    
    func getStreamingProfile_v2(){
        ProfileAPI().getATC_v2(email, id: newListId!, category: "Streaming", completionHandler: { result in
            self.streamingTable.append(["Monday", result.monday])
            self.streamingTable.append(["Tuesday", result.tuesday])
            self.streamingTable.append(["Wednesday", result.wednesday])
            self.streamingTable.append(["Thursday", result.thursday])
            self.streamingTable.append(["Friday", result.friday])
            self.streamingTable.append(["Saturday", result.saturday])
            self.streamingTable.append(["Sunday", result.sunday])
            self.splitTimeTable(table: self.streamingTable)
        })
    }
    
    func getSocialMediaProfile(){
        ProfileAPI().getProfileByCategory(email, profileName: profileName, category: "socialmedia", completionHandler: { result in
            if result.profileId != "" {
                self.socialTable.append(["Monday", result.monday])
                self.socialTable.append(["Tuesday", result.tuesday])
                self.socialTable.append(["Wednesday", result.wednesday])
                self.socialTable.append(["Thursday", result.thursday])
                self.socialTable.append(["Friday", result.friday])
                self.socialTable.append(["Saturday", result.saturday])
                self.socialTable.append(["Sunday", result.sunday])
                
                self.splitTimeTable(table: self.socialTable)
            }
            else {
                self.loadingIndicator.stopAnimating()
                if self.newATCProfile == false {
                    Toast(text: "Something went wrong", duration: Delay.short).show()
                }
                else {
                    self.socialTable.append(["Monday", result.monday])
                    self.socialTable.append(["Tuesday", result.tuesday])
                    self.socialTable.append(["Wednesday", result.wednesday])
                    self.socialTable.append(["Thursday", result.thursday])
                    self.socialTable.append(["Friday", result.friday])
                    self.socialTable.append(["Saturday", result.saturday])
                    self.socialTable.append(["Sunday", result.sunday])
                    
                    self.splitTimeTable(table: self.socialTable)
                }
            }
        })
    }
    
    func getGeneralProfile(){
        ProfileAPI().getProfileByCategory(email, profileName: profileName, category: "general", completionHandler: { result in
            if result.profileId != "" {
                self.generalTable.append(["Monday", result.monday])
                self.generalTable.append(["Tuesday", result.tuesday])
                self.generalTable.append(["Wednesday", result.wednesday])
                self.generalTable.append(["Thursday", result.thursday])
                self.generalTable.append(["Friday", result.friday])
                self.generalTable.append(["Saturday", result.saturday])
                self.generalTable.append(["Sunday", result.sunday])
                self.splitTimeTable(table: self.generalTable)
            }
            else {
                self.loadingIndicator.stopAnimating()
                if self.newATCProfile == false {
                    Toast(text: "Something went wrong", duration: Delay.short).show()
                }
                else {
                    self.generalTable.append(["Monday", result.monday])
                    self.generalTable.append(["Tuesday", result.tuesday])
                    self.generalTable.append(["Wednesday", result.wednesday])
                    self.generalTable.append(["Thursday", result.thursday])
                    self.generalTable.append(["Friday", result.friday])
                    self.generalTable.append(["Saturday", result.saturday])
                    self.generalTable.append(["Sunday", result.sunday])
                    
                    self.splitTimeTable(table: self.generalTable)
                }
            }
        })
    }
    
    func getFacebookProfile(){
        ProfileAPI().getProfileByCategory(email, profileName: profileName, category: "facebook", completionHandler: { result in
            if result.profileId != "" {
                self.fbTable.append(["Monday", result.monday])
                self.fbTable.append(["Tuesday", result.tuesday])
                self.fbTable.append(["Wednesday", result.wednesday])
                self.fbTable.append(["Thursday", result.thursday])
                self.fbTable.append(["Friday", result.friday])
                self.fbTable.append(["Saturday", result.saturday])
                self.fbTable.append(["Sunday", result.sunday])
                
                self.splitTimeTable(table: self.fbTable)
            }
            else {
                self.loadingIndicator.stopAnimating()
                if self.newATCProfile == false {
                    Toast(text: "Something went wrong", duration: Delay.short).show()
                }
                else {
                    self.fbTable.append(["Monday", result.monday])
                    self.fbTable.append(["Tuesday", result.tuesday])
                    self.fbTable.append(["Wednesday", result.wednesday])
                    self.fbTable.append(["Thursday", result.thursday])
                    self.fbTable.append(["Friday", result.friday])
                    self.fbTable.append(["Saturday", result.saturday])
                    self.fbTable.append(["Sunday", result.sunday])
                    
                    self.splitTimeTable(table: self.fbTable)
                }
            }
        })
    }
    
    func getYoutubeProfile(){
        ProfileAPI().getProfileByCategory(email, profileName: profileName, category: "youtube", completionHandler: { result in
            if result.profileId != "" {
                self.ytTable.append(["Monday", result.monday])
                self.ytTable.append(["Tuesday", result.tuesday])
                self.ytTable.append(["Wednesday", result.wednesday])
                self.ytTable.append(["Thursday", result.thursday])
                self.ytTable.append(["Friday", result.friday])
                self.ytTable.append(["Saturday", result.saturday])
                self.ytTable.append(["Sunday", result.sunday])
                
                self.splitTimeTable(table: self.ytTable)
            }
            else {
                self.loadingIndicator.stopAnimating()
                if self.newATCProfile == false {
                    Toast(text: "Something went wrong", duration: Delay.short).show()
                }
                else {
                    self.ytTable.append(["Monday", result.monday])
                    self.ytTable.append(["Tuesday", result.tuesday])
                    self.ytTable.append(["Wednesday", result.wednesday])
                    self.ytTable.append(["Thursday", result.thursday])
                    self.ytTable.append(["Friday", result.friday])
                    self.ytTable.append(["Saturday", result.saturday])
                    self.ytTable.append(["Sunday", result.sunday])
                    
                    self.splitTimeTable(table: self.ytTable)
                }
            }
        })
    }
    
    func getStreamingProfile(){
        ProfileAPI().getProfileByCategory(email, profileName: profileName, category: "streaming", completionHandler: { result in
            if result.profileId != "" {
                self.streamingTable.append(["Monday", result.monday])
                self.streamingTable.append(["Tuesday", result.tuesday])
                self.streamingTable.append(["Wednesday", result.wednesday])
                self.streamingTable.append(["Thursday", result.thursday])
                self.streamingTable.append(["Friday", result.friday])
                self.streamingTable.append(["Saturday", result.saturday])
                self.streamingTable.append(["Sunday", result.sunday])
                
                self.splitTimeTable(table: self.streamingTable)
            }
            else {
                self.loadingIndicator.stopAnimating()
                if self.newATCProfile == false {
                    Toast(text: "Something went wrong", duration: Delay.short).show()
                }
                else {
                    self.streamingTable.append(["Monday", result.monday])
                    self.streamingTable.append(["Tuesday", result.tuesday])
                    self.streamingTable.append(["Wednesday", result.wednesday])
                    self.streamingTable.append(["Thursday", result.thursday])
                    self.streamingTable.append(["Friday", result.friday])
                    self.streamingTable.append(["Saturday", result.saturday])
                    self.streamingTable.append(["Sunday", result.sunday])
                    
                    self.splitTimeTable(table: self.streamingTable)
                }
            }
        })
    }
    
    func getGamingProfile(){
        ProfileAPI().getProfileByCategory(email, profileName: profileName, category: "gaming", completionHandler: { result in
            if result.profileId != "" {
                self.gamingTable.append(["Monday", result.monday])
                self.gamingTable.append(["Tuesday", result.tuesday])
                self.gamingTable.append(["Wednesday", result.wednesday])
                self.gamingTable.append(["Thursday", result.thursday])
                self.gamingTable.append(["Friday", result.friday])
                self.gamingTable.append(["Saturday", result.saturday])
                self.gamingTable.append(["Sunday", result.sunday])
                
                self.splitTimeTable(table: self.gamingTable)
            }
            else {
                self.loadingIndicator.stopAnimating()
                if self.newATCProfile == false {
                    Toast(text: "Something went wrong", duration: Delay.short).show()
                }
                else {
                    self.gamingTable.append(["Monday", result.monday])
                    self.gamingTable.append(["Tuesday", result.tuesday])
                    self.gamingTable.append(["Wednesday", result.wednesday])
                    self.gamingTable.append(["Thursday", result.thursday])
                    self.gamingTable.append(["Friday", result.friday])
                    self.gamingTable.append(["Saturday", result.saturday])
                    self.gamingTable.append(["Sunday", result.sunday])
                    
                    self.splitTimeTable(table: self.gamingTable)
                }
            }
        })
    }

    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func splitTimeTable(table: [[String]]){
        
        for dayy in table {
            print("dayyy ",dayy)
            
            if dayy[1] != "x" {
                var day = dayy[1].components(separatedBy: ",")
                
                for d in day {
                    print("mmmmm \(d)")
                    let start = d.index(d.startIndex, offsetBy: 6)
                    let end = d.index(d.endIndex, offsetBy: -3)
                    let range = start..<end
                    
                    var from = String(d.characters.prefix(2))
                    var to = d.substring(with: range)
                    
                    switch dayy[0] {
                    case "Monday":
                        mondayTable.append("\(from) - \(to)")
                    case "Tuesday":
                        tuesdayTable.append("\(from) - \(to)")
                    case "Wednesday":
                        wednesdayTable.append("\(from) - \(to)")
                    case "Thursday":
                        thursdayTable.append("\(from) - \(to)")
                    case "Friday":
                        fridayTable.append("\(from) - \(to)")
                    case "Saturday":
                        saturdayTable.append("\(from) - \(to)")
                    case "Sunday":
                        sundayTable.append("\(from) - \(to)")
                    default:
                        break
                    }
                }
                
            }
            tableView.reloadData()
            loadingIndicator.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return mondayTable.count
        case 1:
            return tuesdayTable.count
        case 2:
            return wednesdayTable.count
        case 3:
            return thursdayTable.count
        case 4:
            return fridayTable.count
        case 5:
            return saturdayTable.count
        case 6:
            return sundayTable.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "hoursCell", for: indexPath)
        
        cell.textLabel?.textColor = UIColor(red:0.95, green:0.26, blue:0.21, alpha:1.0)
        
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = mondayTable[indexPath.row]
        case 1:
            cell.textLabel?.text = tuesdayTable[indexPath.row]
        case 2:
            cell.textLabel?.text = wednesdayTable[indexPath.row]
        case 3:
            cell.textLabel?.text = thursdayTable[indexPath.row]
        case 4:
            cell.textLabel?.text = fridayTable[indexPath.row]
        case 5:
            cell.textLabel?.text = saturdayTable[indexPath.row]
        case 6:
            cell.textLabel?.text = sundayTable[indexPath.row]
        default:
            break
        }
        
        return cell
    }
    
    func addToDay(sender: UIButton){
        let chosenDay = sender.tag
        
        selectedDay = daysSection[sender.tag]
        timePicker.becomeFirstResponder()
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! EditATCHeaderTableViewCell
        headerCell.contentView.backgroundColor = UIColor(red: (247/255), green: (247/255), blue: (247/255), alpha: 1)
        //headerCell.contentView.layer.borderWidth = 1
        //headerCell.contentView.layer.borderColor = UIColor(red:0.27, green:0.47, blue:0.95, alpha:1.0).cgColor
        
        let borderBottom = UIView(frame: CGRect(x:0, y:54, width: tableView.bounds.size.width, height: 1.0))
        borderBottom.backgroundColor = UIColor(red:0.27, green:0.47, blue:0.95, alpha:1.0)
        headerCell.contentView.addSubview(borderBottom)
        
        switch (section) {
        case 0:
            headerCell.day.text = "Monday"
            headerCell.addNew.tag = 0
        case 1:
            headerCell.day.text = "Tuesday"
            headerCell.addNew.tag = 1
        case 2:
            headerCell.day.text = "Wednesday"
            headerCell.addNew.tag = 2
        case 3:
            headerCell.day.text = "Thursday"
            headerCell.addNew.tag = 3
        case 4:
            headerCell.day.text = "Friday"
            headerCell.addNew.tag = 4
        case 5:
            headerCell.day.text = "Saturday"
            headerCell.addNew.tag = 5
        case 6:
            headerCell.day.text = "Sunday"
            headerCell.addNew.tag = 6
        default:
            headerCell.day.text = "Test"
        }
        
        headerCell.addNew.addTarget(self, action: #selector(EditATCViewController.addToDay), for: .touchUpInside)
        
        return headerCell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let cell = tableView.cellForRow(at: indexPath)
            let time = cell?.textLabel?.text
            
            let alertController = UIAlertController(title: "Delete", message: "Are you sure you want to remove \(time!) time range?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                print("jaaaaa ",indexPath.section)
                switch indexPath.section {
                case 0:
                    self.mondayTable.remove(at: indexPath.row)
                    tableView.reloadData()
                    
                    if self.version == 1 {
                        self.saveToDB()
                    }
                    else {
                        self.saveToDB_v2()
                    }
                case 1:
                    self.tuesdayTable.remove(at: indexPath.row)
                    tableView.reloadData()
                    
                    if self.version == 1 {
                        self.saveToDB()
                    }
                    else {
                        self.saveToDB_v2()
                    }
                case 2:
                    self.wednesdayTable.remove(at: indexPath.row)
                    tableView.reloadData()
                    
                    if self.version == 1 {
                        self.saveToDB()
                    }
                    else {
                        self.saveToDB_v2()
                    }
                case 3:
                    self.thursdayTable.remove(at: indexPath.row)
                    tableView.reloadData()
                    
                    if self.version == 1 {
                        self.saveToDB()
                    }
                    else {
                        self.saveToDB_v2()
                    }
                case 4:
                    self.fridayTable.remove(at: indexPath.row)
                    tableView.reloadData()
                    
                    if self.version == 1 {
                        self.saveToDB()
                    }
                    else {
                        self.saveToDB_v2()
                    }
                case 5:
                    self.saturdayTable.remove(at: indexPath.row)
                    tableView.reloadData()
                    
                    if self.version == 1 {
                        self.saveToDB()
                    }
                    else {
                        self.saveToDB_v2()
                    }
                case 6:
                    self.sundayTable.remove(at: indexPath.row)
                    tableView.reloadData()
                    
                    if self.version == 1 {
                        self.saveToDB()
                    }
                    else {
                        self.saveToDB_v2()
                    }
                default:
                    break
                }
            
            }
            
            let noAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
        }
            
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch pickerView {
        case pickDay:
            return 1
        case pickTime:
            return 2
        case pickCategory:
            return 1
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case pickDay:
            return 7
        case pickTime:
            return times.count
        case pickCategory:
            return categories.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case pickDay:
            return daysSection[row]
        case pickTime:
            return times[row]
        case pickCategory:
            return categories[row]
        default:
            return ""
        }
    }
    
    
    func saveToDB_v2(){
        
        var saveJSON: [String:Any] = [:]
        
        var monday = "", tuesday = "", wednesday = "", thursday = "", friday = "", saturday = "", sunday = ""
        
        if mondayTable.count == 0 {
            monday = "x"
        }
        else{
            
            for m in mondayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                monday = monday+od+":00_"+doo+":00,"
            }
            monday = String(monday.characters.dropLast(1))
        }
        
        if tuesdayTable.count == 0 {
            tuesday = "x"
        }
        else{
            
            for m in tuesdayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                tuesday = tuesday+od+":00_"+doo+":00,"
            }
            tuesday = String(tuesday.characters.dropLast(1))
        }
        
        
        if wednesdayTable.count == 0 {
            wednesday = "x"
        }
        else{
            
            for m in wednesdayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                wednesday = wednesday+od+":00_"+doo+":00,"
            }
            wednesday = String(wednesday.characters.dropLast(1))
        }
        
        
        if thursdayTable.count == 0 {
            thursday = "x"
        }
        else{
            
            for m in thursdayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                thursday = thursday+od+":00_"+doo+":00,"
            }
            thursday = String(thursday.characters.dropLast(1))
        }
        
        if fridayTable.count == 0 {
            friday = "x"
        }
        else{
            
            for m in fridayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                friday = friday+od+":00_"+doo+":00,"
            }
            friday = String(friday.characters.dropLast(1))
        }
        
        if saturdayTable.count == 0 {
            saturday = "x"
        }
        else{
            
            for m in saturdayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                saturday = saturday+od+":00_"+doo+":00,"
            }
            saturday = String(saturday.characters.dropLast(1))
        }
        
        if sundayTable.count == 0 {
            sunday = "x"
        }
        else{
            
            for m in sundayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                sunday = sunday+od+":00_"+doo+":00,"
            }
            sunday = String(sunday.characters.dropLast(1))
        }
        
        var type = ""
        
        switch selectedCategory {
        case 0:
            type = "general"
        case 1:
            type = "facebook"
        case 2:
            type = "youtube"
        case 3:
            type = "streaming"
        case 4:
            type = "gaming"
        case 5:
            type = "socialMedia"
        default:
            break
        }
        
        saveJSON = ["op":"update_mobile",
                    "companyId":email,
                    "filterId":newListId!,
                    type: [
                "Monday":monday,
                "Tuesday":tuesday,
                "Wednesday":wednesday,
                "Thursday":thursday,
                "Friday":friday,
                "Saturday":saturday,
                "Sunday":sunday
            ]]
        
        ProfileAPI().accessTimeControl_v2(email, op: "update_mobile", json: saveJSON, completionHandler: { result, r2 in
            if result == true {
                
                //when saved update all clients
                ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                    if result == true {
                        Toast(text: "Success!", duration: Delay.short).show()
                    }
                    else {
                        Toast(text: "Something went wrong!", duration: Delay.short).show()
                    }
                })
            }
            else {
                Toast(text: "Someting went wrong", duration: Delay.short).show()
            }
        })

    }
    
    func saveToDB(){
        
        print("sejvam")
        var saveString = ""
        
        if mondayTable.count == 0 {
            saveString = saveString+"x/"
        }
        else{
            
            for m in mondayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                saveString = saveString+od+":00_"+doo+":00,"
            }
            saveString = String(saveString.characters.dropLast(1))
            saveString = saveString+"/"
        }
        
        if tuesdayTable.count == 0 {
            saveString = saveString+"x/"
        }
        else {
            for m in tuesdayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                saveString = saveString+od+":00_"+doo+":00,"
            }
            saveString = String(saveString.characters.dropLast(1))
            saveString = saveString+"/"
        }
        
        if wednesdayTable.count == 0 {
            saveString = saveString+"x/"
        }
        else {
            for m in wednesdayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                saveString = saveString+od+":00_"+doo+":00,"
            }
            
            saveString = String(saveString.characters.dropLast(1))
            saveString = saveString+"/"
        }
        
        if thursdayTable.count == 0 {
            saveString = saveString+"x/"
        }
        else {
            for m in thursdayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                saveString = saveString+od+":00_"+doo+":00,"
            }
            
            saveString = String(saveString.characters.dropLast(1))
            saveString = saveString+"/"
        }
        
        if fridayTable.count == 0 {
            saveString = saveString+"x/"
        }
        else {
            for m in fridayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                saveString = saveString+od+":00_"+doo+":00,"
            }
            
            saveString = String(saveString.characters.dropLast(1))
            saveString = saveString+"/"
        }
        
        if saturdayTable.count == 0 {
            saveString = saveString+"x/"
        }
        else {
            for m in saturdayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                saveString = saveString+od+":00_"+doo+":00,"
            }
            
            saveString = String(saveString.characters.dropLast(1))
            saveString = saveString+"/"
        }
        
        if sundayTable.count == 0 {
            saveString = saveString+"x"
        }
        else {
            for m in sundayTable {
                let od = String(m.characters.prefix(2))
                let doo = String(m.characters.suffix(2))
                
                saveString = saveString+od+":00_"+doo+":00,"
            }
            
            saveString = String(saveString.characters.dropLast(1))
            print("saveee ",saveString)
        }
        
        var type = ""
        
        switch selectedCategory {
        case 0:
            type = "general"
        case 1:
            type = "facebook"
        case 2:
            type = "youtube"
        case 3:
            type = "streaming"
        case 4:
            type = "gaming"
        case 5:
            type = "socialmedia"
        default:
            break
        }
        
        ProfileAPI().setATprofile(email, acName: profileName, type: type, data: saveString, completionHandler: { result in
            if result == true {
                
                //when saved update all clients
                ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                    if result == true {
                        Toast(text: "Success!", duration: Delay.short).show()
                    }
                    else {
                        Toast(text: "Something went wrong!", duration: Delay.short).show()
                    }
                })
            }
            else {
                Toast(text: "Someting went wrong", duration: Delay.short).show()
            }
        })
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case pickDay:
            selectedDay = daysSection[row]
            self.view.endEditing(true)
        case pickTime:
            if component == 0 {
                from = Int(times[row])!
                print("from je ",times[row])
                print("from je ",from)
            } else {
                to = Int(times[row])!
            }
        case pickCategory:
            mondayTable.removeAll()
            tuesdayTable.removeAll()
            wednesdayTable.removeAll()
            thursdayTable.removeAll()
            fridayTable.removeAll()
            saturdayTable.removeAll()
            sundayTable.removeAll()
            
            switch row {
            case 0:
                selectedCategory = row
                loadingIndicator.startAnimating()
                generalTable.removeAll()
                
                if version == 1 {
                    getGeneralProfile()
                }
                else {
                    getGeneralProfile_v2()
                }
                
            case 1:
                selectedCategory = row
                loadingIndicator.startAnimating()
                fbTable.removeAll()
                
                if version == 1 {
                    getFacebookProfile()
                }
                else {
                    getFacebookProfile_v2()
                }
                
            case 2:
                selectedCategory = row
                loadingIndicator.startAnimating()
                ytTable.removeAll()
                
                if version == 1 {
                    getYoutubeProfile()
                }
                else {
                    getYoutubeProfile_v2()
                }
            case 3:
                selectedCategory = row
                loadingIndicator.startAnimating()
                streamingTable.removeAll()
                
                if version == 1 {
                    getStreamingProfile()
                }
                else {
                    getStreamingProfile_v2()
                }
            case 4:
                selectedCategory = row
                loadingIndicator.startAnimating()
                gamingTable.removeAll()
                
                if version == 1 {
                    getGamingProfile()
                }
                else {
                    getGamingProfile_v2()
                }
            case 5:
                selectedCategory = row
                loadingIndicator.startAnimating()
                socialTable.removeAll()
                
                if version == 1 {
                    getSocialMediaProfile()
                }
                else {
                    getSocialMediaProfile_v2()
                }
            default:
                break
            }
            selectCategoryBtn.setTitle(categories[row], for: .normal)
            self.view.endEditing(true)
        default:
            break
        }
    }
    
    
    func saveTime(){
        self.view.endEditing(true)
        
        if from == nil && to == nil {
            from = 0
            to = 0
        }
        else if from == nil {
            from = 0
        }
        else if to == nil {
            to = 0
        }
        
        print(from," --- ",to)
        
        if from!<to! {
            
            var append = false
            
            
            
            switch selectedDay {
            case "Monday"?:
                //if mondaytable is empty, there is no need to check if time is overlapping
                if mondayTable.count == 0 {
                    if from!<10 || to!<10 {
                        if from!<10 && to!<10{
                            mondayTable.append("0\(from!) - 0\(to!)")
                        }
                        else if from!>9 {
                            mondayTable.append("\(from!) - 0\(to!)")
                        }
                        else{
                            mondayTable.append("0\(from!) - \(to!)")
                        }
                    }
                    else{
                        print("appendam1")
                        mondayTable.append("\(from!) - \(to!)")
                    }
                    
                    tableView.reloadData()
                    
                    if version == 1 {
                        saveToDB()
                    }
                    else {
                        saveToDB_v2()
                    }
                }
                else{
                    for (index, m) in mondayTable.enumerated() {
                        let od = Int(String(m.characters.prefix(2)))
                        let doo = Int(String(m.characters.suffix(2)))
                        
                        print(mondayTable[index])
                        
                        
                        //let range = od!+1...doo!-1
                        //let range2 = from!...to!
                        
                        var range = 0...1
                        var range2 = 0...1
                        
                        if od!+1>doo!-1{
                            range = od!+1...doo!
                            range2 = from!...to!
                        }
                        else {
                            range = od!+1...doo!-1
                            range2 = from!...to!
                        }
                        
                        if range.overlaps(range2){
                            Toast(text: "Overlap", duration: Delay.short).show()
                            break
                        }
                        else {
                            append = true
                            print(append," ---- ",index," ---- ",mondayTable.count-1)
                            
                            if append == true && index == mondayTable.count-1 {
                                
                                if from!<10 || to!<10 {
                                    if from!<10 && to!<10{
                                        mondayTable.append("0\(from!) - 0\(to!)")
                                    }
                                    else if from!>9 {
                                        mondayTable.append("\(from!) - 0\(to!)")
                                    }
                                    else{
                                        mondayTable.append("0\(from!) - \(to!)")
                                    }
                                }
                                else{
                                    print("appendam1")
                                    mondayTable.append("\(from!) - \(to!)")
                                }
                                
                                var mondayTableTemp = bubbleSort(array: mondayTable)
                                mondayTable = mondayTableTemp
                                
                                tableView.reloadData()
                                
                                if version == 1 {
                                    saveToDB()
                                }
                                else {
                                    saveToDB_v2()
                                }
                                
                                break
                                
                            }
                        }
                        
                    }
                    
                }
            case "Tuesday"?:
                if tuesdayTable.count == 0 {
                    if from!<10 || to!<10 {
                        if from!<10 && to!<10{
                            tuesdayTable.append("0\(from!) - 0\(to!)")
                        }
                        else if from!>9 {
                            tuesdayTable.append("\(from!) - 0\(to!)")
                        }
                        else{
                            tuesdayTable.append("0\(from!) - \(to!)")
                        }
                    }
                    else{
                        print("appendam1")
                        tuesdayTable.append("\(from!) - \(to!)")
                    }
                    
                    tableView.reloadData()
                    
                    if version == 1 {
                        saveToDB()
                    }
                    else {
                        saveToDB_v2()
                    }
                }
                    
                else {
                    for (index, m) in tuesdayTable.enumerated() {
                        print("evoo "+m)
                        let od = Int(String(m.characters.prefix(2)))
                        print("od je ",od)
                        let doo = Int(String(m.characters.suffix(2)))
                        
                        var range = 0...1
                        var range2 = 0...1
                        
                        if od!+1>doo!-1{
                            range = od!+1...doo!
                            range2 = from!...to!
                        }
                        else {
                            range = od!+1...doo!-1
                            range2 = from!...to!
                        }
                        
                        if range.overlaps(range2){
                            Toast(text: "Overlap", duration: Delay.short).show()
                            break
                        }
                        else{
                            append = true
                            
                            if append == true && index == tuesdayTable.count-1 {
                                
                                if from!<10 || to!<10 {
                                    if from!<10 && to!<10{
                                        tuesdayTable.append("0\(from!) - 0\(to!)")
                                    }
                                    else if from!>9 {
                                        tuesdayTable.append("\(from!) - 0\(to!)")
                                    }
                                    else{
                                        tuesdayTable.append("0\(from!) - \(to!)")
                                    }
                                }
                                else{
                                    print("appendam1")
                                    tuesdayTable.append("\(from!) - \(to!)")
                                }
                                
                                var mondayTableTemp = bubbleSort(array: tuesdayTable)
                                tuesdayTable = mondayTableTemp
                                
                                tableView.reloadData()
                                
                                if version == 1 {
                                    saveToDB()
                                }
                                else {
                                    saveToDB_v2()
                                }
                                
                                break
                                
                            }
                            
                        }
                        
                    }
                }
                
            case "Wednesday"?:
                if wednesdayTable.count == 0 {
                    if from!<10 || to!<10 {
                        if from!<10 && to!<10{
                            wednesdayTable.append("0\(from!) - 0\(to!)")
                        }
                        else if from!>9 {
                            wednesdayTable.append("\(from!) - 0\(to!)")
                        }
                        else{
                            wednesdayTable.append("0\(from!) - \(to!)")
                        }
                    }
                    else{
                        print("appendam1")
                        wednesdayTable.append("\(from!) - \(to!)")
                    }
                    
                    tableView.reloadData()
                    
                    if version == 1 {
                        saveToDB()
                    }
                    else {
                        saveToDB_v2()
                    }
                }
                else {
                    for (index, m) in wednesdayTable.enumerated() {
                        print("evoo "+m)
                        let od = Int(String(m.characters.prefix(2)))
                        print("od je ",od)
                        let doo = Int(String(m.characters.suffix(2)))
                        
                        //let range = od!+1...doo!-1
                        //let range2 = from!...to!
                        
                        var range = 0...1
                        var range2 = 0...1
                        
                        if od!+1>doo!-1{
                            range = od!+1...doo!
                            range2 = from!...to!
                        }
                        else {
                            range = od!+1...doo!-1
                            range2 = from!...to!
                        }
                        
                        if range.overlaps(range2){
                            Toast(text: "Overlap", duration: Delay.short).show()
                            break
                        }
                        else{
                            append = true
                            print(append," ---- ",index," ---- ",wednesdayTable.count-1)
                            
                            if append == true && index == wednesdayTable.count-1 {
                                
                                if from!<10 || to!<10 {
                                    if from!<10 && to!<10{
                                        wednesdayTable.append("0\(from!) - 0\(to!)")
                                    }
                                    else if from!>9 {
                                        wednesdayTable.append("\(from!) - 0\(to!)")
                                    }
                                    else{
                                        wednesdayTable.append("0\(from!) - \(to!)")
                                    }
                                }
                                else{
                                    print("appendam1")
                                    wednesdayTable.append("\(from!) - \(to!)")
                                }
                                
                                var mondayTableTemp = bubbleSort(array: wednesdayTable)
                                wednesdayTable = mondayTableTemp
                                
                                tableView.reloadData()
                                
                                if version == 1 {
                                    saveToDB()
                                }
                                else {
                                    saveToDB_v2()
                                }
                                
                                break
                                
                            }
                            
                        }
                        
                    }
                }
                
            case "Thursday"?:
                if thursdayTable.count == 0 {
                    if from!<10 || to!<10 {
                        if from!<10 && to!<10{
                            thursdayTable.append("0\(from!) - 0\(to!)")
                        }
                        else if from!>9 {
                            thursdayTable.append("\(from!) - 0\(to!)")
                        }
                        else{
                            thursdayTable.append("0\(from!) - \(to!)")
                        }
                    }
                    else{
                        print("appendam1")
                        thursdayTable.append("\(from!) - \(to!)")
                    }
                    
                    tableView.reloadData()
                    
                    if version == 1 {
                        saveToDB()
                    }
                    else {
                        saveToDB_v2()
                    }
                }
                else {
                    for (index, m) in thursdayTable.enumerated() {
                        print("evoo "+m)
                        let od = Int(String(m.characters.prefix(2)))
                        print("od je ",od)
                        let doo = Int(String(m.characters.suffix(2)))
                        
                        //let range = od!+1...doo!-1
                        //let range2 = from!...to!
                        
                        var range = 0...1
                        var range2 = 0...1
                        
                        if od!+1>doo!-1{
                            range = od!+1...doo!
                            range2 = from!...to!
                        }
                        else {
                            range = od!+1...doo!-1
                            range2 = from!...to!
                        }
                        
                        if range.overlaps(range2){
                            Toast(text: "Overlap", duration: Delay.short).show()
                            break
                        }
                        else{
                            append = true
                            print(append," ---- ",index," ---- ",thursdayTable.count-1)
                            
                            if append == true && index == thursdayTable.count-1 {
                                
                                if from!<10 || to!<10 {
                                    if from!<10 && to!<10{
                                        thursdayTable.append("0\(from!) - 0\(to!)")
                                    }
                                    else if from!>9 {
                                        thursdayTable.append("\(from!) - 0\(to!)")
                                    }
                                    else{
                                        thursdayTable.append("0\(from!) - \(to!)")
                                    }
                                }
                                else{
                                    print("appendam1")
                                    thursdayTable.append("\(from!) - \(to!)")
                                }
                                
                                var mondayTableTemp = bubbleSort(array: thursdayTable)
                                thursdayTable = mondayTableTemp
                                
                                tableView.reloadData()
                                
                                if version == 1 {
                                    saveToDB()
                                }
                                else {
                                    saveToDB_v2()
                                }
                                
                                break
                                
                            }
                            
                        }
                        
                    }
                    
                }
            case "Friday"?:
                if fridayTable.count == 0 {
                    if from!<10 || to!<10 {
                        if from!<10 && to!<10{
                            fridayTable.append("0\(from!) - 0\(to!)")
                        }
                        else if from!>9 {
                            fridayTable.append("\(from!) - 0\(to!)")
                        }
                        else{
                            fridayTable.append("0\(from!) - \(to!)")
                        }
                    }
                    else{
                        print("appendam1")
                        fridayTable.append("\(from!) - \(to!)")
                    }
                    
                    tableView.reloadData()
                    
                    if version == 1 {
                        saveToDB()
                    }
                    else {
                        saveToDB_v2()
                    }
                }
                    
                else {
                    for (index, m) in fridayTable.enumerated() {
                        print("evoo "+m)
                        let od = Int(String(m.characters.prefix(2)))
                        print("od je ",od)
                        let doo = Int(String(m.characters.suffix(2)))
                        
                        print("rangeeee ",od!," --- ",doo!)
                        
                        
                        
                        //let range = od!+1...doo!-1
                        //let range2 = from!...to!
                        
                        var range = 0...1
                        var range2 = 0...1
                        
                        if od!+1>doo!-1{
                            range = od!+1...doo!
                            range2 = from!...to!
                        }
                        else {
                            range = od!+1...doo!-1
                            range2 = from!...to!
                        }
                        
                        if range.overlaps(range2){
                            Toast(text: "Overlap", duration: Delay.short).show()
                            break
                        }
                        else{
                            append = true
                            print(append," ---- ",index," ---- ",fridayTable.count-1)
                            
                            if append == true && index == fridayTable.count-1 {
                                
                                if from!<10 || to!<10 {
                                    if from!<10 && to!<10{
                                        fridayTable.append("0\(from!) - 0\(to!)")
                                    }
                                    else if from!>9 {
                                        fridayTable.append("\(from!) - 0\(to!)")
                                    }
                                    else{
                                        fridayTable.append("0\(from!) - \(to!)")
                                    }
                                }
                                else{
                                    print("appendam1")
                                    fridayTable.append("\(from!) - \(to!)")
                                }
                                
                                var mondayTableTemp = bubbleSort(array: fridayTable)
                                fridayTable = mondayTableTemp
                                
                                tableView.reloadData()
                                
                                if version == 1 {
                                    saveToDB()
                                }
                                else {
                                    saveToDB_v2()
                                }
                                
                                break
                                
                            }
                            
                        }
                        
                    }
                }
                
            case "Saturday"?:
                
                if saturdayTable.count == 0 {
                    if from!<10 || to!<10 {
                        if from!<10 && to!<10{
                            saturdayTable.append("0\(from!) - 0\(to!)")
                        }
                        else if from!>9 {
                            saturdayTable.append("\(from!) - 0\(to!)")
                        }
                        else{
                            saturdayTable.append("0\(from!) - \(to!)")
                        }
                    }
                    else{
                        print("appendam1")
                        saturdayTable.append("\(from!) - \(to!)")
                    }
                    
                    tableView.reloadData()
                    
                    if version == 1 {
                        saveToDB()
                    }
                    else {
                        saveToDB_v2()
                    }
                }
                else {
                    for (index, m) in saturdayTable.enumerated() {
                        print("evoo "+m)
                        let od = Int(String(m.characters.prefix(2)))
                        print("od je ",od)
                        let doo = Int(String(m.characters.suffix(2)))
                        
                        //let range = od!+1...doo!-1
                        //let range2 = from!...to!
                    
                        
                        var range = 0...1
                        var range2 = 0...1
                        
                        if od!+1>doo!-1{
                            range = od!+1...doo!
                            range2 = from!...to!
                        }
                        else {
                            range = od!+1...doo!-1
                            range2 = from!...to!
                        }
                        
                        if range.overlaps(range2){
                            Toast(text: "Overlap", duration: Delay.short).show()
                            break
                        }
                        else{
                            append = true
                            print(append," ---- ",index," ---- ",saturdayTable.count-1)
                            
                            if append == true && index == saturdayTable.count-1 {
                                
                                if from!<10 || to!<10 {
                                    if from!<10 && to!<10{
                                        saturdayTable.append("0\(from!) - 0\(to!)")
                                    }
                                    else if from!>9 {
                                        saturdayTable.append("\(from!) - 0\(to!)")
                                    }
                                    else{
                                        saturdayTable.append("0\(from!) - \(to!)")
                                    }
                                }
                                else{
                                    print("appendam1")
                                    saturdayTable.append("\(from!) - \(to!)")
                                }
                                
                                var mondayTableTemp = bubbleSort(array: saturdayTable)
                                saturdayTable = mondayTableTemp
                                
                                tableView.reloadData()
                                
                                if version == 1 {
                                    saveToDB()
                                }
                                else {
                                    saveToDB_v2()
                                }
                                
                                break
                                
                            }
                            
                        }
                        
                    }
                }
            case "Sunday"?:
                if sundayTable.count == 0 {
                    if from!<10 || to!<10 {
                        if from!<10 && to!<10{
                            sundayTable.append("0\(from!) - 0\(to!)")
                        }
                        else if from!>9 {
                            sundayTable.append("\(from!) - 0\(to!)")
                        }
                        else{
                            sundayTable.append("0\(from!) - \(to!)")
                        }
                    }
                    else{
                        print("appendam1")
                        sundayTable.append("\(from!) - \(to!)")
                    }
                    
                    tableView.reloadData()
                    
                    
                    if version == 1 {
                        saveToDB()
                    }
                    else {
                        saveToDB_v2()
                    }
                }
                else {
                    for (index, m) in sundayTable.enumerated() {
                        print("evoo "+m)
                        let od = Int(String(m.characters.prefix(2)))
                        print("od je ",od)
                        let doo = Int(String(m.characters.suffix(2)))
                        
                        //let range = od!+1...doo!-1
                        //let range2 = from!...to!
                        
                        var range = 0...1
                        var range2 = 0...1
                        
                        if od!+1>doo!-1{
                            range = od!+1...doo!
                            range2 = from!...to!
                        }
                        else {
                            range = od!+1...doo!-1
                            range2 = from!...to!
                        }
                        
                        if range.overlaps(range2){
                            Toast(text: "Overlap", duration: Delay.short).show()
                            break
                        }
                        else{
                            append = true
                            print(append," ---- ",index," ---- ",sundayTable.count-1)
                            
                            if append == true && index == sundayTable.count-1 {
                                
                                if from!<10 || to!<10 {
                                    if from!<10 && to!<10{
                                        sundayTable.append("0\(from!) - 0\(to!)")
                                    }
                                    else if from!>9 {
                                        sundayTable.append("\(from!) - 0\(to!)")
                                    }
                                    else{
                                        sundayTable.append("0\(from!) - \(to!)")
                                    }
                                }
                                else{
                                    print("appendam1")
                                    sundayTable.append("\(from!) - \(to!)")
                                }
                                
                                var mondayTableTemp = bubbleSort(array: sundayTable)
                                sundayTable = mondayTableTemp
                                
                                tableView.reloadData()
                                
                                if version == 1 {
                                    saveToDB()
                                }
                                else {
                                    saveToDB_v2()
                                }
                                
                                break
                            }
                        }
                    }
                }
                
            default:
                break
            }
            
        }else {
            Toast(text: "Select different time range!", duration: Delay.short).show()
        }

    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
