//
//  EditBWlistViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Toaster

class EditBWlistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SendAlert {
    
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var createNewView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var urlSwitch: UISwitch!
    @IBOutlet var url: UITextField!
    @IBOutlet var addNewUrlBtn: UIButton!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    
    
    let defaults = UserDefaults.standard
    var email: String = ""
    var urls: [BWlist] = []
    
    var bw: BW?
    
    var version: Int?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FirstViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FirstViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        navBar.topItem?.title = "Edit "+(bw?.name)!
        
        url.delegate = self
        
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        if version == 1 {
            getURLs()
        }
        else {
            getURLs_v2()
        }
        
        self.view.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.95, alpha:1.0)
        createNewView.layer.borderWidth = 1
        createNewView.layer.borderColor = UIColor.lightGray.cgColor
        createNewView.layer.cornerRadius = 5
        
        url.placeholder = "Enter URL"
        addNewUrlBtn.tintColor = UIColor.white
        addNewUrlBtn.layer.cornerRadius = 12
        addNewUrlBtn.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
        
        urlSwitch.tintColor = .red
        urlSwitch.backgroundColor = .red
        urlSwitch.layer.cornerRadius = 16
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        print("tappp")
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: NSNotification){
        tableView.isUserInteractionEnabled = false
    }
    
    func keyboardWillHide(_ notification: NSNotification){
        tableView.isUserInteractionEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addURLtoList(textField)
        return true;
    }
    
    func getURLs(){
        BWlistAPI().getURLsForList(email, name: (bw?.name)!, completionHandler: { result in
            self.urls = result
            self.tableView.reloadData()
            self.loadingIndicator.stopAnimating()
        })
    }
    
    func getURLs_v2(){
        BWlistAPI().getURLs_v2(email, listName: (bw?.name)!, completionHandler: { result in
            self.urls = result
            self.tableView.reloadData()
            self.loadingIndicator.stopAnimating()
        })
    }
    
    func removeURL(email: String, listName: String, url: String){
        BWlistAPI().removeFromList(email, list: listName, url: url, completionHandler: { result in
            if result == true {
                ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                    if result == true {
                        self.getURLs()
                        Toast(text: "Success!", duration: Delay.short).show()
                    }
                    else {
                        Toast(text: "Something went wrong!", duration: Delay.short).show()
                    }
                })
            }
            else {
                Toast(text: "Something went wrong!", duration: Delay.short).show()
            }
        })
    }
    
    func removeURL_v2(email: String, id: String, url: String){
        BWlistAPI().BWList_v2(email, id: id, op: "removeUrl", url: url, completionHandler: { result, r2 in
            if result == true {
                ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                    if result == true {
                        self.getURLs_v2()
                        Toast(text: "Success!", duration: Delay.short).show()
                    }
                    else {
                        Toast(text: "Something went wrong!", duration: Delay.short).show()
                    }
                })
            }
            else {
                Toast(text: "Something went wrong!", duration: Delay.short).show()
            }
        })
    }
    
    @IBAction func addURLtoList(_ sender: Any) {
        var action = ""
        if (url.text?.characters.count)! > 0 {
            
            if urlSwitch.isOn == true {
                action = "0"
            }
            else {
                action = "1"
            }
            
            if version == 1 {
                BWlistAPI().addNewURL(email, list: (bw?.name)!, url: url.text!, action: action, completionHandler: { result in
                    if result == true {
                        self.url.text = ""
                        self.getURLs()
                        
                        ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                            if result == true {
                                Toast(text: "Success!", duration: Delay.short).show()
                            }
                            else {
                                Toast(text: "Something went wrong1!", duration: Delay.short).show()
                            }
                        })
                    }
                    else {
                        Toast(text: "Something went wrong2!", duration: Delay.short).show()
                    }
                })
            }
            else {
                BWlistAPI().BWList_v2(email, id: (bw?.id)!, op: "add", url: url.text!, action: action, completionHandler: { result, r2 in
                    if result == true {
                        self.url.text = ""
                        self.getURLs_v2()
                        
                        ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                            if result == true {
                                Toast(text: "Success!", duration: Delay.short).show()
                            }
                            else {
                                Toast(text: "Something went wrong3!", duration: Delay.short).show()
                            }
                        })
                    }
                    else {
                        Toast(text: "Something went wrong4!", duration: Delay.short).show()
                    }
                })
            }
        }
        else {
            Toast(text: "URL is too short!", duration: Delay.short).show()
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return urls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "urls", for: indexPath) as! BWlistTableViewCell
        
        cell.delegate = self
        
        cell.url.text = urls[indexPath.row].url
        
        if version == 1 {
            cell.action.bwListName = bw?.name
        }
        else {
            cell.action.bwListName = bw?.id
        }
        
        cell.action.url = urls[indexPath.row].url
        cell.action.email = email
        
        cell.version = version
        
        if urls[indexPath.row].action == true {
            cell.action.isOn = true
        }
        else {
            cell.action.tintColor = .red
            cell.action.backgroundColor = .red
            cell.action.layer.cornerRadius = 16
            cell.action.isOn = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete) {
            let cell = tableView.cellForRow(at: indexPath) as! BWlistTableViewCell
            
            let alertController = UIAlertController(title: "Delete URL", message: "Are you sure you want to delete \(cell.url.text!)?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                if self.version == 1 {
                    self.removeURL(email: self.email, listName: self.bw!.name, url: self.urls[indexPath.row].url)
                }
                else {
                    self.removeURL_v2(email: self.email, id: self.bw!.id, url: self.urls[indexPath.row].url)
                }
            }
            
            let noAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func newAlert(status: Bool) {
        if status == true {
            ExtensionAPI().updateAllExtensions(self.email, completionHandler: { result in
                if result == true {
                    Toast(text: "Success!", duration: Delay.short).show()
                    
                    if self.version == 1 {
                        self.getURLs()
                    }
                    else {
                        self.getURLs_v2()
                    }
                }
                else {
                    Toast(text: "Something went wrong!", duration: Delay.short).show()
                }
            })
        }
        else{
            Toast(text: "Failure!", duration: Delay.short).show()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
