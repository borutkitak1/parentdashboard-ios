//
//  ProfileAPI.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class OtherAPI {
    
    let baseURL: String = "https://service.block.si/config/"
    
    
    func checkStudentDomain(_ domain: String, completionHandler: @escaping ((Bool, String) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"checkDomain/"+domain+"/2", method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true, responseJSON["CompanyId"].stringValue)
                    
                }
                else {
                    completionHandler(false, responseJSON["CompanyId"].stringValue)
                }
            }
            else {
                completionHandler(false, "false")
            }
            
            
        }
    }
    
    func recoverPassword(_ email: String, token: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"setAccessToken/"+email+"/"+token+"/eeparent", method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
        }
    }
    
}
