//
//  LostPasswordModalViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 23/08/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Toaster

class LostPasswordModalViewController: UIViewController {
    
    
    @IBOutlet var modalView: UIView!
    @IBOutlet var email: UITextField!
    @IBOutlet var sendEmailBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        email.placeholder = "Enter your e-mail address"
        modalView.layer.cornerRadius = 7
        sendEmailBtn.layer.cornerRadius = 7
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendRecoveryEmail(_ sender: Any) {
        var token = UUID().uuidString
        token = token.replacingOccurrences(of: "-", with: "")
        
        if (email.text?.characters.count)! > 0 {
            OtherAPI().recoverPassword(email.text!, token: token, completionHandler: { result in
                if result == true {
                    Toast(text: "An e-mail was sent to \(self.email.text!) with further instructions.", duration: Delay.short).show()
                    self.dismiss(animated: true, completion: nil)
                }
                else {
                    Toast(text: "Something went wrong!", duration: Delay.short).show()
                }
            })
        } else {
            Toast(text: "E-mail field should not be empty!", duration: Delay.short).show()
        }
        
    }

    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
