//
//  BWTableViewCell.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 07/11/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class BWTableViewCell: UITableViewCell {
    
    var bw: BW?
    var profile: Profile?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
