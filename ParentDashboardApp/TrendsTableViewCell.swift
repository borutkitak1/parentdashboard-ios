//
//  TrendsTableViewCell.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 24/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class TrendsTableViewCell: UITableViewCell {
    
    @IBOutlet var unreviewed_issues: UILabel!
    @IBOutlet var issues_nr: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
