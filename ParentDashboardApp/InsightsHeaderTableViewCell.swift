//
//  InsightsHeaderTableViewCell.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 01/09/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class InsightsHeaderTableViewCell: UITableViewCell {

    @IBOutlet var url: UILabel!
    @IBOutlet var hits: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
