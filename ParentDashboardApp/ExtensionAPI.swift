//
//  ProfileAPI.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ExtensionAPI {
    
    let baseURL: String = "https://service.block.si/config/"

    
    func updateAllExtensions(_ email: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"updateExtensions/"+email+"/null/null/eeparent", method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func getExtension(_ companyId: String, studentEmail: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"getExtension/"+companyId+"/"+studentEmail, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                print("stat ",responseJSON)
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }
    
    func getUsers_V3(_ companyId: String, studentEmail: String, completionHandler: @escaping ((Bool) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"getUsers_v3/"+companyId+"/"+studentEmail, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                print("stat ",responseJSON)
                if responseJSON["status"] == "true" {
                    
                    completionHandler(true)
                    
                }
                else {
                    completionHandler(false)
                }
            }
            else {
                completionHandler(false)
            }
            
            
        }
    }

}
