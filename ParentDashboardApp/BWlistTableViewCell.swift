//
//  BWlistTableViewCell.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 10/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit


protocol SendAlert {
    func newAlert(status: Bool)
}

class BWlistTableViewCell: UITableViewCell {
    
    var delegate: SendAlert!

    @IBOutlet var url: UILabel!
    @IBOutlet var action: URLswitch!
    var version: Int?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func switchChanged(_ sender: Any) {
        let uiswitch = sender as! URLswitch
        
        
        var action = ""
        
        if uiswitch.isOn == true {
            action = "0"
        }else{
            action = "1"
            uiswitch.tintColor = .red
            uiswitch.backgroundColor = .red
            uiswitch.layer.cornerRadius = 16
        }
        
        if version == 1 {
            BWlistAPI().addURL(uiswitch.email, list: uiswitch.bwListName, url: uiswitch.url, action: action, completionHandler: { result in
                if result == true {
                    self.delegate.newAlert(status: true)
                }
                else{
                    self.delegate.newAlert(status: false)
                }
            })
        }
        else {
            
            BWlistAPI().BWList_v2(uiswitch.email, id: uiswitch.bwListName, op: "update", url: uiswitch.url, action: action, completionHandler: { result, r2 in
                if result == true {
                    self.delegate.newAlert(status: true)
                }
                else{
                    self.delegate.newAlert(status: false)
                }
            })
        }
    }

}
