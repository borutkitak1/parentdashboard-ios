//
//  CreateNewUserModalViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 23/08/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Toaster
import CryptoSwift

class CreateNewUserModalViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var createAccBtn: UIButton!
    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var retypePassword: UITextField!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var studentEmail: UITextField!
    
    var p = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        createAccBtn.layer.cornerRadius = 7
        
        email.placeholder = "Enter your e-mail address"
        password.placeholder = "Choose a password"
        retypePassword.placeholder = "Confirm Password"
        firstName.placeholder = "First Name"
        lastName.placeholder = "Last Name"
        phone.placeholder = "Phone"
        studentEmail.placeholder = "Student e-mail (optional)"
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func keyboardWillShow(notification:NSNotification) {
        guard let keyboardFrameValue = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue else {
            return
        }
        let keyboardFrame = view.convert(keyboardFrameValue.cgRectValue, from: nil)
        scrollView.contentOffset = CGPoint(x:0, y:keyboardFrame.size.height-100)
    }
    
    func keyboardWillHide(notification:NSNotification) {
        scrollView.contentOffset = .zero
    }
    
    
    func tap(gesture: UITapGestureRecognizer) {
        print("tappp")
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createAccount(_ sender: Any) {
        let email = self.email.text!
        let pass = self.password.text!
        let pass2 = self.retypePassword.text!
        let fName = self.firstName.text!
        let lName = self.lastName.text!
        let phone = self.phone.text!
        let studentMail = self.studentEmail.text!
        
        if ValidateEmailHelper().isValidEmail(testStr: email){
            if email.characters.count > 0 && pass.characters.count > 0 && pass2.characters.count > 0 && fName.characters.count > 0 && lName.characters.count > 0 && phone.characters.count > 0 {
                
                var domain = [String]()
                
                if pass == pass2 {
                    if studentMail.characters.count > 0 {
                        if studentMail.contains("@") {
                            domain = studentMail.components(separatedBy: "@")
                            
                            OtherAPI().checkStudentDomain(domain[1], completionHandler: { result, email2 in
                                if result == true {
                                    Toast(text: "The domain of the student account you entered is not a BMEE customer.", duration: Delay.short).show()
                                }
                                else {
                                    ExtensionAPI().getExtension(email2, studentEmail: studentMail, completionHandler: { result in
                                        if result == true || result == false {
                                            ExtensionAPI().getUsers_V3(email2, studentEmail: studentMail, completionHandler: { result in
                                                if result == true {
                                                    UsersAPI().createBlocksiParentAcc(email, password: pass.md5(), firstname: fName, lastName: lName, phone: phone, completionHandler: { result in
                                                        if result == true {
                                                            UsersAPI().requestParentAccount(email, studentId: studentMail, completionHandler: { result in
                                                                
                                                            })
                                                            
                                                            UsersAPI().sendWelcomeMsg(email, firstName: fName, completionHandler: { result in
                                                                if result == true {
                                                                    Toast(text: "Welcome message sent!", duration: Delay.short).show()
                                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                                                        self.dismiss(animated: true, completion: nil)
                                                                    }
                                                                }
                                                            })
                                                        }
                                                        else {
                                                            Toast(text: "Something went wrong!", duration: Delay.short).show()
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                        else {
                                            Toast(text: "The specified student account is invalid.", duration: Delay.short).show()
                                        }
                                    })
                                }
                            })
                        }
                    }
                    else {
                        UsersAPI().createBlocksiParentAcc(email, password: pass.md5(), firstname: fName, lastName: lName, phone: phone, completionHandler: { result in
                            if result == true {
                                UsersAPI().sendWelcomeMsg(email, firstName: fName, completionHandler: { result in
                                    if result == true {
                                        Toast(text: "Welcome message sent!", duration: Delay.short).show()
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                            self.dismiss(animated: true, completion: nil)
                                        }
                                    }
                                })
                            }
                            else {
                                Toast(text: "Something went wrong!", duration: Delay.short).show()
                            }
                        })
                    }
                    
                }
                else {
                    Toast(text: "Passwords do not match!", duration: Delay.short).show()
                }
                
            }
            else {
                Toast(text: "You must fill all fileds, except last!", duration: Delay.short).show()
            }
        }
        else {
            Toast(text: "Wrong email format!", duration: Delay.short).show()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ""
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
