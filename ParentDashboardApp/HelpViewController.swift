//
//  HelpViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 26/10/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    
    
    @IBOutlet weak var helpView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        helpView.layer.cornerRadius = 7

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeHelp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func gotIt(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
