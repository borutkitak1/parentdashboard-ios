//
//  DashboardViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 10/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Toaster
import SwiftDate
import GoogleSignIn
import Font_Awesome_Swift

class DashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var email: String = ""
    var defaults = UserDefaults.standard
    
    var allowed: Int = 0
    var blocked: Int = 0
    var warning: Int = 0
    
    var allowedT: Int = 0
    var blockedT: Int = 0
    var warningT: Int = 0
    
    var stats: [Int] = [0,0,0]
    var todayStats: [Int] = [0,0,0]
    
    var userExists = false
    
    
    @IBOutlet weak var trendsView: UIView!
    @IBOutlet weak var unreviewedTrendsNr: UILabel!
    @IBOutlet weak var badge: UIView!
    @IBOutlet var logoutBtn: UIBarButtonItem!
    
    @IBOutlet weak var viewTrendsBtn: UIButton!
    
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        ////////////
        viewTrendsBtn.layer.borderWidth = 1
        viewTrendsBtn.layer.borderColor = UIColor(red:0.74, green:0.74, blue:0.74, alpha:1.0).cgColor
        viewTrendsBtn.layer.cornerRadius = 7
        
        
        badge.layer.cornerRadius = 17.5
        badge.clipsToBounds = true
        
        logoutBtn.FAIcon = FAType.FASignOut
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        let date = Date()-1.day
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        var startDate = formatter.string(from: date)
        
        
        allowed = 0
        blocked = 0
        warning = 0
        allowedT = 0
        blockedT = 0
        warningT = 0
        
        UsersAPI().getStudentsForParentAccount(email, completionHandler: { result in
            if result.count != 0 {
                for (index, i) in result.enumerated() {
                    if i.status == true {
                        self.userExists = true
                        AnalyticsAPI().elasticSearch(i.companyId, startDate: startDate, studentId: i.studentId, timezone: "none", completionHandler: { al, bl, war in
                            self.allowed = self.allowed + al
                            self.blocked = self.blocked + bl
                            self.warning = self.warning + war
                            
                            if index == result.count-1 {
                                print(index," ---- ",result.count)
                                self.stats.removeAll()
                                self.stats.append(self.allowed)
                                self.stats.append(self.blocked)
                                self.stats.append(self.warning)
                                self.tableView.reloadData()
                                self.loadingIndicator.stopAnimating()
                            }
                            
                            print("allowed ",self.allowed)
                        })
                    }
                }
            }
            else {
                self.loadingIndicator.stopAnimating()
                //Toast(text: "Could not load data!", duration: Delay.short).show()
            }
        })
        
        let date2 = Date()
        let formatter2 = DateFormatter()
        
        formatter2.dateFormat = "yyyy-MM-dd"
        
        var startDate2 = formatter.string(from: date2)
        
        UsersAPI().getStudentsForParentAccount(email, completionHandler: { result in
            if result.count != 0 {
                for (index, i) in result.enumerated() {
                    if i.status == true {
                        self.userExists = true
                        AnalyticsAPI().elasticSearch(i.companyId, startDate: startDate2, studentId: i.studentId, timezone: "none", completionHandler: { al, bl, war in
                            //print("printaaaj \(al)   \(bl)  \(war)")
                            self.allowedT = self.allowedT + al
                            self.blockedT = self.blockedT + bl
                            self.warningT = self.warningT + war
                            
                            if index == result.count-1 {
                                //print(index," ---- ",result.count)
                                self.todayStats.removeAll()
                                self.todayStats.append(self.allowedT)
                                self.todayStats.append(self.blockedT)
                                self.todayStats.append(self.warningT)
                                self.tableView.reloadData()
                                self.loadingIndicator.stopAnimating()
                            }
                            
                        })
                    }
                }
            }
            else {
                self.loadingIndicator.stopAnimating()
            }
        })
        
        AnalyticsAPI().getTimezone(email, completionHandler: { result in
            if result != "none" {
                
            }
        })
        
        UsersAPI().getStudentsForParentAccount(email, completionHandler: { result in
            if result.count != 0 {
                var unreviewedIssuesSum = 0
                
                if result[0].companyId != "" {
                    for student in result {
                        var domain = student.studentId.components(separatedBy: "@")
                        TrendsAPI().getHomeUsersByAdmin(student.studentId, domain: domain[1], completionHandler: { result, result2, url in
                            
                            if result.count != 0 {
                                unreviewedIssuesSum = unreviewedIssuesSum + Int(result[0])!
                            }
                                
                            /**
                            else {
                                unreviewedIssuesSum = 0
                            }
                            
                            if unreviewedIssuesSum != 0 {
                                self.reviewTrendsBtn.backgroundColor = UIColor(red:0.27, green:0.47, blue:0.95, alpha:1.0)
                            }
                            **/
                            
                            if unreviewedIssuesSum != 0 {
                                self.unreviewedTrendsNr.text = "\(unreviewedIssuesSum)"
                                UIView.transition(with: self.badge, duration: 0.3, options: .transitionCrossDissolve, animations: {()->Void in
                                    self.badge.isHidden = false
                                }, completion: nil)
                            }
                            
                        })
                    }
                    if self.loadingIndicator.isAnimating {
                        self.loadingIndicator.stopAnimating()
                    }
                }
                else {
                    Toast(text: "Data is not available!", duration: Delay.short).show()
                    self.loadingIndicator.stopAnimating()
                }
            }
            else {
                Toast(text: "Could not load data!", duration: Delay.short).show()
            }
        })
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openTrends(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
    }
    
    
    @IBAction func logout(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default)
        {
            (result : UIAlertAction) -> Void in
            
            self.loadingIndicator.startAnimating()
            
            GIDSignIn.sharedInstance().signOut()
            self.defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            
            if self.defaults.string(forKey: "email") == nil {
                Toast(text: "Success", duration: Delay.short).show()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    
                    let loginVC: UIViewController? = self.storyboard?.instantiateViewController(withIdentifier: "login") as? LoginViewController
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = loginVC
                }
            }
            else {
                Toast(text: "Something went wrong!", duration: Delay.short).show()
            }
        }
        
        let noAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            (result : UIAlertAction) -> Void in
        }
        
        alertController.addAction(okAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100
        }
        else if indexPath.section == 0 && indexPath.row == 1 {
            return 30
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "stats", for: indexPath) as! RecentStatsTableViewCell
            
            switch indexPath.section {
            case 0:
                cell.allow.text = String(todayStats[0])
                cell.block.text = String(todayStats[1])
                
                var lineView = UIView(frame: CGRect(x: 0, y: 99, width: cell.contentView.bounds.size.width, height: 1))
                lineView.backgroundColor = UIColor(red:0.27, green:0.47, blue:0.95, alpha:1.0)
                cell.contentView.addSubview(lineView)
                
                var lineView2 = UIView(frame: CGRect(x: cell.contentView.bounds.size.width/2, y: 0, width: 1, height: cell.contentView.bounds.size.height))
                lineView2.backgroundColor = UIColor(red:0.27, green:0.47, blue:0.95, alpha:1.0)
                cell.contentView.addSubview(lineView2)
            case 1:
                cell.allow.text = String(stats[0])
                cell.block.text = String(stats[1])
                
                //add line to bottom of cell
                var lineView = UIView(frame: CGRect(x: 0, y: 99, width: cell.contentView.bounds.size.width, height: 1))
                lineView.backgroundColor = UIColor(red:0.74, green:0.74, blue:0.74, alpha:1.0)
                cell.contentView.addSubview(lineView)
                
                
                var lineView2 = UIView(frame: CGRect(x: cell.contentView.bounds.size.width/2, y: 0, width: 1, height: cell.contentView.bounds.size.height))
                lineView2.backgroundColor = UIColor(red:0.74, green:0.74, blue:0.74, alpha:1.0)
                cell.contentView.addSubview(lineView2)
            default:
                break
            }
            
            return cell
        }
        else {
            return tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "header") as! RecentStatsHeaderTableViewCell
        
        
        if section == 0 {
            headerCell.contentView.backgroundColor = UIColor(red:0.27, green:0.47, blue:0.95, alpha:1.0)
            headerCell.day.text = "Today"
        }
        else {
            headerCell.contentView.backgroundColor = UIColor(red:0.74, green:0.74, blue:0.74, alpha:1.0)
            headerCell.day.text = "Yesterday"
        }
        
        return headerCell.contentView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48.0
    }

}
