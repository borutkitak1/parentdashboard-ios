//
//  UsersAPI.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 03/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GroupsAPI {
    
    let baseURL: String = "https://service.block.si/config/"
    
    
    func getAllGroups(_ email: String, completionHandler: @escaping (([Group]) -> Void)) -> Void {
        var groups: [Group] = []
        
        Alamofire.request(baseURL+"getAllGroups/"+email, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            //print(response.data)     // server data
            print(response.result)   // result of response serialization
            //print(response)
            
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    for i in responseJSON["Groups"] {
                        let companyId = i.1["CompanyId"].stringValue
                        let groupId = i.1["GroupId"].stringValue
                        let filterId = i.1["FilterId"].stringValue
                        let ytFilterId = i.1["YTFilterId"].stringValue
                        let atProfileId = i.1["ATProfileId"].stringValue
                        let listId = i.1["ListId"].stringValue
                        let regexId = i.1["RegExId"].stringValue
                        let ytChannelId = i.1["YTChannelListId"].stringValue
                        let ytKeywordsId = i.1["YTKeywordsId"].stringValue
                        let safeSearch = i.1["SafeSearch"].boolValue
                        let blockURL = i.1["BlockUrl"].stringValue
                        let appFilterId = i.1["AppFilterId"].stringValue
                        
                        groups.append(Group(companyId: companyId, groupId: groupId, filterId: filterId, ytFilterId: ytFilterId, atProfileId: atProfileId, listId: listId, regexId: regexId, ytChannelListId: ytChannelId, ytKeywordsId: ytKeywordsId, safeSearch: safeSearch, blockURL: blockURL, appFilterId: appFilterId))
                    }
                    completionHandler(groups)
                }
                    
                else {
                    completionHandler(groups)
                }
                
                
            }
            else {
                completionHandler(groups)
            }
            
            
        }
    }
}
