//
//  LoginViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 03/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import GoogleSignIn
import CryptoSwift
import Toaster

class LoginViewController: UIViewController, GIDSignInUIDelegate {
    
    let defaults = UserDefaults.standard
    
    @IBOutlet var googleImage: UIImageView!
    @IBOutlet var googleLoginBtn: UIButton!
    //@IBOutlet var gLoginView: GIDSignInButton!
    @IBOutlet var drawLine: UIView!
    @IBOutlet weak var ima: UIImageView!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var createNewAccBtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        loadingIndicator.hidesWhenStopped = true
        
        email.placeholder = "email@example.com"
        password.placeholder = "Password"
        loginBtn.layer.cornerRadius = 7
        
        createNewAccBtn.layer.zPosition = 1
        
        view.bringSubview(toFront: createNewAccBtn)
        
        
        createNewAccBtn.isUserInteractionEnabled = true
        
        //gLoginView.style = .wide
        
        //gLoginView.addTarget(self, action: #selector(login), for: .touchDown)
        
        
        googleLoginBtn.setImage(UIImage(named:"google_login"), for: [])
        
        googleLoginBtn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        googleLoginBtn.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        googleLoginBtn.layer.shadowOpacity = 0.3
        googleLoginBtn.layer.shadowRadius = 10.0
        

        
        //googleImage.image = UIImage(named: "google_login")
        
        if UIScreen.main.nativeBounds.height == 1136 {
        
        }
        else {
            let coloredSquare = UIView()
            
            coloredSquare.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
            
            coloredSquare.frame = CGRect(x: 60, y: view.frame.maxY-40, width: 180, height: 180)
            coloredSquare.layer.cornerRadius = coloredSquare.frame.width / 2
            
            self.view.addSubview(coloredSquare)
            
            UIView.animate(withDuration: 10.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                
                coloredSquare.frame = CGRect(x: self.view.frame.maxX-30, y: self.view.frame.maxY-200, width: 180, height: 180)
                
            }, completion: nil)
            
            
            ///
            let coloredSquare3 = UIView()
            
            coloredSquare3.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
            
            coloredSquare3.frame = CGRect(x: 90, y: view.frame.maxY-40, width: 100, height: 100)
            coloredSquare3.layer.cornerRadius = coloredSquare3.frame.width / 2
            
            self.view.addSubview(coloredSquare3)
            
            UIView.animate(withDuration: 3.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                
                coloredSquare3.frame = CGRect(x: 150, y: self.view.frame.maxY-60, width: 100, height: 100)
                
            }, completion: nil)
            
            ///
            let coloredSquare4 = UIView()
            
            coloredSquare4.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
            
            coloredSquare4.frame = CGRect(x: 150, y: view.frame.maxY-80, width: 170, height: 170)
            coloredSquare4.layer.cornerRadius = coloredSquare4.frame.width / 2
            
            self.view.addSubview(coloredSquare4)
            
            UIView.animate(withDuration: 3.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                
                coloredSquare4.frame = CGRect(x: 200, y: self.view.frame.maxY-80, width: 170, height: 170)
                
            }, completion: nil)
            
            ///
            let coloredSquare5 = UIView()
            
            coloredSquare5.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
            
            coloredSquare5.frame = CGRect(x: 240, y: view.frame.maxY-80, width: 180, height: 180)
            coloredSquare5.layer.cornerRadius = coloredSquare5.frame.width / 2
            
            self.view.addSubview(coloredSquare5)
            
            UIView.animate(withDuration: 3.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                
                coloredSquare5.frame = CGRect(x: 280, y: self.view.frame.maxY-140, width: 180, height: 180)
                
            }, completion: nil)
            
            ///
            let coloredSquare6 = UIView()
            
            coloredSquare6.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
            
            coloredSquare6.frame = CGRect(x: 200, y: view.frame.maxY-40, width: 60, height: 60)
            coloredSquare6.layer.cornerRadius = coloredSquare6.frame.width / 2
            
            self.view.addSubview(coloredSquare6)
            
            UIView.animate(withDuration: 5.0, delay: 0, options: [.repeat], animations: {
                
                coloredSquare6.frame = CGRect(x: 220, y: self.view.frame.maxY-280, width: 60, height: 60)
                coloredSquare6.alpha = 0
                
            }, completion: nil)
            
            ///
            let coloredSquare7 = UIView()
            
            coloredSquare7.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
            
            coloredSquare7.frame = CGRect(x: view.frame.maxX-80, y: view.frame.maxY-80, width: 130, height: 130)
            coloredSquare7.layer.cornerRadius = coloredSquare7.frame.width / 2
            
            self.view.addSubview(coloredSquare7)
            
            UIView.animate(withDuration: 4.0, delay: 1.0, options: [.repeat, .autoreverse], animations: {
                
                coloredSquare7.frame = CGRect(x: self.view.frame.maxX-30, y: self.view.frame.maxY-230, width: 130, height: 130)
                
            }, completion: nil)
            

        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        print("tappp")
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func gLogin(_ sender: Any) {
        loadingIndicator.startAnimating()
        GIDSignIn.sharedInstance().signIn()
    }
    
    func login() {
        print("test")
        loadingIndicator.startAnimating()
    }
    
    
    @IBAction func blocksiLogin(_ sender: Any) {
        if (email.text?.characters.count)! > 0 && (password.text?.characters.count)! > 0 {
            loadingIndicator.startAnimating()
            let email2 = self.email.text
            let password = self.password.text?.md5()
            
            UsersAPI().blocksiAccLogin(email2!, password: password!, completionHandler: { result in
                if result == true {
                    self.loadingIndicator.stopAnimating()
                    
                    self.defaults.set(email2!, forKey: "email")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let tabbarVC = storyboard.instantiateViewController(withIdentifier: "tabController") as! UITabBarController
                    self.present(tabbarVC, animated: true, completion: nil)
                }
                else {
                    self.email.text = ""
                    self.password.text = ""
                    self.loadingIndicator.stopAnimating()
                    Toast(text: "Something went wrong!", duration: Delay.short).show()
                }
            })
        }
        else {
            Toast(text: "You must fill all fields!", duration: Delay.short).show()
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "loggedIn"){
            if let des = segue.destination as? UITabBarController{
                des.selectedIndex = 0
            }
        }
    }
    
    func createCircleAnimation(startX: Int, startY: Int, endX: Int, endY: Int){
        let square2 = UIView()
        
        square2.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
        
        square2.frame = CGRect(x: 0, y: view.frame.maxY-110, width: 120, height: 120)
        square2.layer.cornerRadius = square2.frame.width / 2
        
        self.view.addSubview(square2)
        
        UIView.animate(withDuration: 5.0, delay: 1.0, options: [.repeat, .autoreverse], animations: {
            
            square2.frame = CGRect(x: self.view.frame.maxX-30, y: self.view.frame.maxY-90, width: 100, height: 100)
            
        }, completion: nil)
    }
 

}
