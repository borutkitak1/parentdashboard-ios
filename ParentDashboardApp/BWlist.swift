//
//  BWlist.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation

class BWlist {
    
    var list: String
    var url: String
    var action: Bool
    
    init(list: String, url: String, action: Bool){
        self.list = list
        self.url = url
        self.action = action
    }
    
}
