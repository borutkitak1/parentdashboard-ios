//
//  RecentStatsTableViewCell.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 11/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class RecentStatsTableViewCell: UITableViewCell {
   
    @IBOutlet var allow: UILabel!
    @IBOutlet var block: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
