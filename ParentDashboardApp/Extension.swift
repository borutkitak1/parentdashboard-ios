//
//  Extension.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 03/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

class Extension {
    var companyId: String
    var userId: String?
    var groupId: String?
    var connStatus: Bool?
    
    init(companyId: String, userId: String, groupId: String, connStatus: Bool){
        self.companyId = companyId
        self.userId = userId
        self.groupId = groupId
        self.connStatus = connStatus
    }
    
    init(companyId: String){
        self.companyId = companyId
    }
}
