//
//  Student.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

class Student {

    var companyId: String
    var parentId: String
    var studentId: String
    var bmVersion: String
    var profile: String
    var status: Bool
    var atProfile: String
    var bwList: String
    
    init(companyId: String, parentId: String, studentId: String, bmVersion: String, profile: String, status: Bool, atProfile: String, bwList: String){
        self.companyId = companyId
        self.parentId = parentId
        self.studentId = studentId
        self.bmVersion = bmVersion
        self.profile = profile
        self.status = status
        self.atProfile = atProfile
        self.bwList = bwList
    }
    
}
