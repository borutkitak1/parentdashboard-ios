//
//  AnalyticsAPI.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//
import Alamofire
import SwiftyJSON

class AnalyticsAPI {

    let baseURL: String = "https://service.block.si/config/"
    let elasticsearchUrl = "https://service.block.si/extension_analytics"
    
    func elasticSearch(_ email: String, startDate: String, studentId: String, timezone: String, completionHandler: @escaping ((Int, Int, Int) -> Void)) -> Void {
        
        var allowed: Int = 0
        var blocked: Int = 0
        var warning: Int = 0
        
        
        Alamofire.request(elasticsearchUrl+"?company="+email+"&date="+startDate+"&endDate="+startDate+"&QueryType=count&user="+studentId+"&timezone="+timezone, method: .get).responseJSON { response in
            //print(response.request)  // original URL request
            //print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            if response.result.isSuccess {
                
                let responseJSON = JSON(response.result.value!)
            
                
                for i in responseJSON["aggregations"]["group_by_action"]["buckets"] {
                    if i.1["key"].stringValue == "0" {
                        allowed = i.1["doc_count"].intValue
                    }
                    if i.1["key"].stringValue == "1" {
                        blocked = i.1["doc_count"].intValue
                    }
                    if i.1["key"].stringValue == "2" {
                        warning = i.1["doc_count"].intValue
                    }
                }
                completionHandler(allowed, blocked, warning)
                
                
            }
            else {
                completionHandler(allowed, blocked, warning)
            }
            
            
        }
    }
    
    func getTimezone(_ email: String, completionHandler: @escaping ((String) -> Void)) -> Void {
        
        Alamofire.request(baseURL+"getTimezone/"+email+"/eeparent", method: .get).responseJSON { response in
            //print(response.request)  // original URL request
            //print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    let timezoneLoc = responseJSON["TimezoneLoc"].stringValue
                    
                    completionHandler(timezoneLoc)
                }
                else {
                    completionHandler("")
                }
                
                
            }
            else {
                completionHandler("")
            }   
        }
    }
    
    func getURLs(_ email: String, startDate: String, endDate: String,studentEmail: String, timezone: String, completionHandler: @escaping (([URLs], [URLs]) -> Void)) -> Void {
        
        var allowedURLs = [URLs]()
        var blockedURLs = [URLs]()
        
        Alamofire.request(elasticsearchUrl+"?company="+email+"&date="+startDate+"&endDate="+endDate+"&user="+studentEmail+"&QueryType=url&timezone="+timezone, method: .get).responseJSON { response in
            print(response.request)  // original URL request
            //print(response.response) // HTTP URL response
            //print(response.data)     // server data
            //print(response.result)   // result of response serialization
            //print(response)
            if response.result.isSuccess {
                let responseJSON = JSON(response.result.value!)
                
                if responseJSON["status"] == "true" {
                    let urls = responseJSON["a"]
                    
                    if urls.count > 0 {
                        for (index, i) in urls[0].enumerated() {
                            if i.1["Type"].intValue == 0 {
                                //allowed
                                if index+1<urls[0].count{
                                    for j in urls[0][index+1].dictionaryObject! {
                                        if j.key == "Type" {}
                                        else{
                                            //print("url \(j.key)")
                                            allowedURLs.append(URLs(url: j.key, hits:"\(j.value)", status: "allowed"))
                                        }
                                    }
                                }
                            }
                            else if i.1["Type"].intValue == 1 {
                                //blocked
                                if index+1<urls[0].count{
                                    for j in urls[0][index+1].dictionaryObject! {
                                        if j.key == "Type" {}
                                        else{
                                            blockedURLs.append(URLs(url: j.key, hits:"\(j.value)", status: "blocked"))
                                        }
                                    }
                                }
                                
                            }
                        }
                        
                        if allowedURLs.count == 0 {
                            allowedURLs.append(URLs(url: "No data!", hits:"", status: "allowed"))
                        }
                        if blockedURLs.count == 0 {
                            blockedURLs.append(URLs(url: "No data!", hits:"", status: "blocked"))
                        }
                        
                        completionHandler(allowedURLs, blockedURLs)
                    }
                    else {
                        blockedURLs.append(URLs(url: "No data!", hits:"", status: "blocked"))
                        allowedURLs.append(URLs(url: "No data!", hits:"", status: "allowed"))
                        completionHandler(allowedURLs, blockedURLs)
                    }
                }
                else {
                    blockedURLs.append(URLs(url: "No data!", hits:"", status: "blocked"))
                    allowedURLs.append(URLs(url: "No data!", hits:"", status: "allowed"))
                    completionHandler(allowedURLs, blockedURLs)
                }
                
            }
            else {
                blockedURLs.append(URLs(url: "No data!", hits:"", status: "blocked"))
                allowedURLs.append(URLs(url: "No data!", hits:"", status: "allowed"))
                completionHandler(allowedURLs, blockedURLs)
            }
        }
    }

}
