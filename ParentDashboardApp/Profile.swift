//
//  Profile.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation

class Profile {
    
    var id: String
    var companyId: String
    var profileId: String
    var type: String
    var monday: String
    var tuesday: String
    var wednesday: String
    var thursday: String
    var friday: String
    var saturday: String
    var sunday: String
    
    init(id: String, companyId: String, profileId: String, type: String, monday: String, tuesday: String, wednesday: String, thursday: String, friday: String, saturday: String, sunday: String){
        
        self.id = id
        self.companyId = companyId
        self.profileId = profileId
        self.type = type
        self.monday = monday
        self.tuesday = tuesday
        self.wednesday = wednesday
        self.thursday = thursday
        self.friday = friday
        self.saturday = saturday
        self.sunday = sunday
        
    }

}
