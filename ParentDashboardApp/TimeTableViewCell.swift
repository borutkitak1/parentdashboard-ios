//
//  TimeTableViewCell.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 17/08/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class TimeTableViewCell: UITableViewCell {

    @IBOutlet var timeFromTo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
