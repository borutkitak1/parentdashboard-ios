//
//  ValidateEmailHelper.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 31/08/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation

class ValidateEmailHelper{
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
}
