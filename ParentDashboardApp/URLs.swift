//
//  URL.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 01/09/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import Foundation

class URLs {
    
    var url: String
    var hits: String
    var status: String
    
    init(url: String, hits: String, status: String){
        self.url = url
        self.hits = hits
        self.status = status
    }
    
}
