//
//  Company.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 05/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

class Company {
    
    var id: String
    var companyId: String
    var subscriptionId: String
    var maxLicense: Int
    var endDate: String
    var bmVersion: String
    var accStatus: Bool
    var startDate: String
    var firstName: String
    var lastName: String
    var phone: String
    var city: String
    var state: String
    var country: String
    var organization: String
    var timezone: String
    var timezoneLoc: String
    
    
    init(id: String, companyId: String, subscriptionId: String, maxLicense: Int, endDate: String, bmVersion: String, accStatus: Bool, startDate: String, firstName: String, lastName: String, phone: String, city: String, state: String, country: String, organization: String, timezone: String, timezoneLoc: String){
        self.id = id
        self.companyId = companyId
        self.subscriptionId = subscriptionId
        self.maxLicense = maxLicense
        self.endDate = endDate
        self.bmVersion = bmVersion
        self.accStatus = accStatus
        self.startDate = startDate
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.city = city
        self.state = state
        self.country = country
        self.organization = organization
        self.timezone = timezone
        self.timezoneLoc = timezoneLoc
        
    }
}
