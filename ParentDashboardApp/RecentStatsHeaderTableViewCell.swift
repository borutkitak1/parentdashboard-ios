//
//  RecentStatsHeaderTableViewCell.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 11/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class RecentStatsHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet var day: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
