//
//  UpdatePasswordModal.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 04/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit

class UpdatePasswordModal: UIViewController {

    @IBOutlet var modalView: UIView!
    @IBOutlet var pass: UITextField!
    @IBOutlet var repeatPass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalView.layer.cornerRadius = 8

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updatePass(_ sender: Any) {
        if pass.text == repeatPass.text {
            if (pass.text?.characters.count)! < 8 {
                print("Password must be at least 8 characters long.")
            }
            else {
                print("do the api call")
            }
        }else{
            print("passwords dont match")
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
