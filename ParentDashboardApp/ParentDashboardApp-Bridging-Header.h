//
//  ParentDashboardApp-Bridging-Header.h
//  ParentDashboardApp
//
//  Created by Borut Kitak on 03/07/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

#ifndef ParentDashboardApp_Bridging_Header_h
#define ParentDashboardApp_Bridging_Header_h
#import <Google/SignIn.h>

#import "SVGKImage.h"
#import "SVGKParser.h"
#import "SVGKLayer.h"
#import "CALayerExporter.h"
#import "SVGKExporterNSData.h"
#import "SVGKExporterUIImage.h"
#import "SVGKImage+CGContext.h"


#endif /* ParentDashboardApp_Bridging_Header_h */
