//
//  FirstViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 30/06/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Toaster
import Font_Awesome_Swift

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var helpBtn: UIBarButtonItem!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var username: UILabel!
    let defaults = UserDefaults.standard
    var email: String = ""
    var users: [Student] = []
    var profiles: [Profile] = []
    var bwList: [BW] = []
    var selectedGroup = ""
    var selectedList = ""
    var student = ""
    
    @IBOutlet var createNewView: UIView!
    
    @IBOutlet var addStudentBtn: UIButton!
    @IBOutlet var newStudentEmail: UITextField!
    @IBOutlet var tableView: UITableView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        helpBtn.FAIcon = .FAInfoCircle
        
        NotificationCenter.default.addObserver(self, selector: #selector(FirstViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FirstViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        UsersAPI().getStudentsForParentAccount(email, completionHandler: { result in
            if result.count != 0 {
                self.users = result
                self.tableView.reloadData()
                self.loadingIndicator.stopAnimating()
                
                if result[0].companyId == ""{
                    self.tableView.isUserInteractionEnabled = false
                    self.performSegue(withIdentifier: "usersHelp", sender: nil)
                }
                else {
                    self.tableView.isUserInteractionEnabled = true
                }
            }
            else {
                self.loadingIndicator.stopAnimating()
                Toast(text: "Could not load data!", duration: Delay.short).show()
            }
        })
        
        ProfileAPI().getAllProfiles(email, completionHandler: { result in
            if result.count > 0 {
                if result[0].profileId != "empty"{
                    self.profiles += result
                }
            }
        })
    
        
        ProfileAPI().getAllProfiles_v2(email, completionHandler: { result in
            if result.count > 0 {
                if result[0].profileId != "empty"{
                    self.profiles += result
                }
            }
        })
        
        BWlistAPI().getBWlists(email, completionHandler: { result in
            if result.count > 0 {
                if result[0].name != ""{
                    self.bwList += result
                }
            }
        })
        
        BWlistAPI().getBWlists_v2(email, completionHandler: { result in
            if result.count > 0 {
                if result[0].name != ""{
                    self.bwList += result
                }
            }
        })
        
        self.view.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.95, alpha:1.0)
        createNewView.layer.borderWidth = 1
        createNewView.layer.borderColor = UIColor.lightGray.cgColor
        createNewView.layer.cornerRadius = 5
        
        newStudentEmail.placeholder = "Enter student email"
        addStudentBtn.tintColor = UIColor.white
        addStudentBtn.layer.cornerRadius = 12
        addStudentBtn.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
        
    }
    

    @IBAction func addStudent(_ sender: Any) {
        if isValidEmail(testStr: newStudentEmail.text!) {
            UsersAPI().requestParentAccount(email, studentId: newStudentEmail.text!, completionHandler: { result in
                if result == true {
                    
                    UsersAPI().getStudentsForParentAccount(self.email, completionHandler: { result in
                        if result.count != 0 {
                            self.users = result
                            self.tableView.reloadData()
                            self.loadingIndicator.stopAnimating()
                        }
                        else {
                            self.loadingIndicator.stopAnimating()
                            Toast(text: "Could not load new users!", duration: Delay.short).show()
                        }
                    })
                    
                    Toast(text: "Request sent to the school admninistrator.", duration: Delay.short).show()
                }
                else {
                    Toast(text: "Something went wrong!", duration: Delay.short).show()
                }
            })
        }
        else{
            Toast(text: "Wrong email format!", duration: Delay.short).show()
        }
    }
    
    
    @IBAction func openHelp(_ sender: Any) {
        performSegue(withIdentifier: "usersHelp", sender: nil)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newStudentEmail.delegate = self
    
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        newStudentEmail.resignFirstResponder()
    }
    
    func keyboardWillShow(_ notification: NSNotification){
        tableView.isUserInteractionEnabled = false
    }
    
    func keyboardWillHide(_ notification: NSNotification){
        tableView.isUserInteractionEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addStudent(textField)
        return true;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! UsersTableViewCell
        
        if cell.policyName.text == "Validated" {
            
            selectedList = cell.student.bwList
            selectedGroup = cell.student.atProfile
            student = cell.student.studentId
            
            performSegue(withIdentifier: "setPolicy", sender: nil)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "user") as! UsersTableViewCell
        
        
        cell.student = users[indexPath.row]
        cell.name.text = users[indexPath.row].studentId
        
        if users[indexPath.row].companyId == "" {
            cell.policyName.isHidden = true
        }
        else {
             cell.policyName.isHidden = false
        }
        
        if users[indexPath.row].status == true {
            cell.policyName.text = "Validated"
        }
        else{
            cell.policyName.text = "Pending"
            cell.policyName.textColor = .red
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete) {
            let cell = tableView.cellForRow(at: indexPath) as! UsersTableViewCell
            
            let alertController = UIAlertController(title: "Delete user", message: "Are you sure you want to delete \(cell.name.text!)?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                
                UsersAPI().getSchoolAdminForParentAccount(self.email, studentEmail: cell.name.text!, completionHandler: { result in
                    if result != "" {
                        UsersAPI().removeParentAcc(result, email: self.email, studentId: cell.name.text!, completionHandler: { result in
                            if result == true {
                                self.users.remove(at: indexPath.row)
                                Toast(text: "User removed!", duration: Delay.short).show()
                                self.tableView.reloadData()
                            }
                            else {
                                Toast(text: "Something went wrong!", duration: Delay.short).show()
                            }
                        })
                    }
                })
                
                
            }
            
            let noAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                (result : UIAlertAction) -> Void in
            }
            
            alertController.addAction(okAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "setPolicy" {
            let des = segue.destination as! ChoosePolicyModal
            des.atprofiles = profiles
            des.list = bwList
            des.selectedList = selectedList
            des.selectedProfile = selectedGroup
            des.studentId = student
        }
        else if segue.identifier == "usersHelp" {
            let des = segue.destination as! UsersHelpViewController
        }
    }

}

