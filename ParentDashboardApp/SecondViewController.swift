//
//  SecondViewController.swift
//  ParentDashboardApp
//
//  Created by Borut Kitak on 30/06/2017.
//  Copyright © 2017 Blocksi. All rights reserved.
//

import UIKit
import Toaster
import Font_Awesome_Swift

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var bwlists: [BW] = []
    var profiles: [Profile] = []
    
    var email: String = ""
    var defaults = UserDefaults.standard
    
    var bwList = ""
    var profileN = ""
    
    var newATCProfile = false
    var deleteATC = false
    
    var version: Int?
    
    var newListId: String = ""
    
    @IBOutlet weak var helpButton: UIBarButtonItem!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var createNewView: UIView!
    @IBOutlet var newFilterName: UITextField!
    @IBOutlet var addFilterBtn: UIButton!
    @IBOutlet var listName: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var segmentControl: UISegmentedControl!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        helpButton.FAIcon = .FAInfoCircle
        
        NotificationCenter.default.addObserver(self, selector: #selector(SecondViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SecondViewController.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if let user = defaults.string(forKey: "email") {
            email = user
        }
        
        getBWlists()
        getATprofiles()
        
        self.view.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.95, alpha:1.0)
        createNewView.layer.borderWidth = 1
        createNewView.layer.borderColor = UIColor.lightGray.cgColor
        createNewView.layer.cornerRadius = 5
        
        newFilterName.placeholder = "Enter new list name"
        addFilterBtn.tintColor = UIColor.white
        addFilterBtn.layer.cornerRadius = 12
        addFilterBtn.backgroundColor = UIColor(red:1.00, green:0.78, blue:0.12, alpha:1.0)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        newFilterName.delegate = self
        
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.startAnimating()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    func tap(gesture: UITapGestureRecognizer) {
        print("tappp")
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: NSNotification){
        tableView.isUserInteractionEnabled = false
    }
    
    func keyboardWillHide(_ notification: NSNotification){
        tableView.isUserInteractionEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        createNew(textField)
        return true;
    }
    
    @IBAction func openHelp(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            performSegue(withIdentifier: "helpSegue", sender: nil)
        case 1:
            performSegue(withIdentifier: "ATCHelpSegue", sender: nil)
        default:
            break
        }
    }
    

    @IBAction func onSegmentChange(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
            listName.text = "BW lists"
            if bwlists.count>0{
                if bwlists[0].name == "" {
                    tableView.isUserInteractionEnabled = false
                    performSegue(withIdentifier: "helpSegue", sender: nil)
                }
                else {
                    tableView.isUserInteractionEnabled = true
                }
            }
            else{
                performSegue(withIdentifier: "helpSegue", sender: nil)
            }
        case 1:
            listName.text = "Access time controls"
            if profiles.count>0{
                if profiles[0].profileId == "" {
                    tableView.isUserInteractionEnabled = false
                    performSegue(withIdentifier: "ATCHelpSegue", sender: nil)
                }
                else {
                    tableView.isUserInteractionEnabled = true
                }
            }else {
               performSegue(withIdentifier: "ATCHelpSegue", sender: nil)
            }
        default:
            break
        }
        tableView.reloadData()
    }
    
    func getBWlists(){
        
        BWlistAPI().getBWlists(email, completionHandler: { result2 in
            if result2.count != 0 {
                
                var temp = result2
                
                if result2.count > 0 && result2[0].name == "" {
                    temp.remove(at: 0)
                }
                
                BWlistAPI().getBWlists_v2(self.email, completionHandler: { result in
                    if result.count != 0 {
                        self.bwlists = temp + result
                        
                        self.tableView.reloadData()
                        self.loadingIndicator.stopAnimating()
                        
                        if self.bwlists.count>0{
                            if self.bwlists[0].name == "" {
                                if self.segmentControl.selectedSegmentIndex == 0 {
                                    self.tableView.isUserInteractionEnabled = false
                                    self.performSegue(withIdentifier: "helpSegue", sender: nil)
                                }
                            }
                            else {
                                self.tableView.isUserInteractionEnabled = true
                            }
                        }
                        else {
                            self.performSegue(withIdentifier: "helpSegue", sender: nil)
                        }
                    }
                    else {
                        self.loadingIndicator.stopAnimating()
                        Toast(text: "Could not load data!", duration: Delay.short).show()
                    }
                })
            }
            else {
                self.loadingIndicator.stopAnimating()
                Toast(text: "Could not load data!", duration: Delay.short).show()
            }
        })
    }
    
    func getATprofiles(){
        ProfileAPI().getAllProfiles(email, completionHandler: { result2 in
            if result2.count != 0 {
                
                var temp = result2
                
                if result2.count > 0 && result2[0].profileId == "empty" {
                    temp.remove(at: 0)
                }
                
                ProfileAPI().getAllProfiles_v2(self.email, completionHandler: { result in
                    if result.count != 0 {
                        
                        var temp2 = result
                        
                        if result.count > 0 && result[0].profileId == "empty" {
                            temp2.remove(at: 0)
                        }
                        
                        self.profiles = temp + temp2
                        
                        self.tableView.reloadData()
                        self.loadingIndicator.stopAnimating()
                        
                        if self.profiles.count>0{
                            if self.profiles[0].profileId == "empty" && self.deleteATC == true {
                                self.tableView.isUserInteractionEnabled = false
                                self.performSegue(withIdentifier: "ATCHelpSegue", sender: nil)
                            }
                            else if self.profiles[0].profileId == "empty" && self.segmentControl.selectedSegmentIndex == 1 {
                                self.tableView.isUserInteractionEnabled = false
                                self.performSegue(withIdentifier: "ATCHelpSegue", sender: nil)
                            }
                        }
                        else {
                            if self.segmentControl.selectedSegmentIndex == 1 {
                                self.performSegue(withIdentifier: "ATCHelpSegue", sender: nil)
                            }
                        }
                        
                    }
                })
                
            }
            else {
                self.loadingIndicator.stopAnimating()
                Toast(text: "Could not load data!", duration: Delay.short).show()
            }
        })
        deleteATC = false
    }

    @IBAction func createNew(_ sender: Any) {
        if (newFilterName.text?.characters.count)! > 4 {
            if !(newFilterName.text?.contains(" "))! {
                switch segmentControl.selectedSegmentIndex {
                case 0:
                    bwList = newFilterName.text!
                    version = 2
                    
                    BWlistAPI().BWList_v2(email, op: "create", urls: [], name: newFilterName.text!, completionHandler: { result, r2 in
                        if result == true {
                            self.newListId = r2!
                            self.performSegue(withIdentifier: "editBWlist", sender: nil)
                            self.newFilterName.text = ""
                        }
                        else {
                           Toast(text: "Something went wrong!", duration: Delay.short).show()
                        }
                    })
                    
                case 1:
                    profileN = newFilterName.text!
                    //newATCProfile = true
                    version = 2
                    
                    ProfileAPI().accessTimeControl_v2(email, op: "create", name: profileN, completionHandler: { result, r2 in
                        if result == true {
                            self.newListId = r2!
                            self.performSegue(withIdentifier: "editATC", sender: nil)
                            self.newFilterName.text = ""
                        }
                        else {
                            Toast(text: "Something went wrong!", duration: Delay.short).show()
                        }
                    })
                    
                default:
                    break
                }
            }else {
                Toast(text: "Do not use space!", duration: Delay.short).show()
            }
        }
        else {
            Toast(text: "Too short name!", duration: Delay.short).show()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentControl.selectedSegmentIndex == 0 {
            return bwlists.count
        }
        return profiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celica", for: indexPath) as! BWTableViewCell
        
        if segmentControl.selectedSegmentIndex == 0 {
            cell.textLabel?.text = bwlists[indexPath.row].name
            cell.bw = bwlists[indexPath.row]
        }
        else {
            cell.textLabel?.text = profiles[indexPath.row].profileId
            cell.profile = profiles[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("klikn na \(indexPath.row)")
        let cell = tableView.cellForRow(at: indexPath) as! BWTableViewCell
        
        switch segmentControl.selectedSegmentIndex {
        case 0:
            bwList = (cell.textLabel?.text)!
            
            if cell.bw?.id == "" {
                version = 1
            }
            else{
                version = 2
            }
            performSegue(withIdentifier: "editBWlist", sender: indexPath.row)
        case 1:
            profileN = (cell.textLabel?.text)!
            
            if cell.profile?.id == "" {
                version = 1
            }
            else{
                version = 2
            }
            performSegue(withIdentifier: "editATC", sender: indexPath.row)
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete) {
            let cell = tableView.cellForRow(at: indexPath) as! BWTableViewCell
            
            let alertController = UIAlertController(title: "Delete list", message: "Are you sure you want to delete \((cell.textLabel?.text)!)?", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                
                switch self.segmentControl.selectedSegmentIndex {
                case 0:
                    if cell.bw?.id != ""{
                        BWlistAPI().BWList_v2(self.email, id: (cell.bw?.id)!, op: "removeFilter", completionHandler: { result, r2 in
                            if result == true {
                                self.getBWlists()
                                Toast(text: "Success!", duration: Delay.short).show()
                            }
                        })
                    }
                    else{
                        BWlistAPI().removeList(self.email, list: (cell.textLabel?.text)!, completionHandler: { result in
                            if result == true {
                                self.getBWlists()
                                Toast(text: "Success!", duration: Delay.short).show()
                            }
                        })
                    }
                case 1:
                    if cell.profile?.id != "" {
                        ProfileAPI().accessTimeControl_v2(self.email, id: (cell.profile?.id)!, op: "remove", completionHandler: { result, r2 in
                            if result == true {
                                self.deleteATC = true
                                self.getATprofiles()
                            }
                        })
                    }
                    else {
                        ProfileAPI().removeATprofile(self.email, list: (cell.textLabel?.text)!, completionHandler: { result in
                            if result == true {
                                self.deleteATC = true
                                self.getATprofiles()
                                Toast(text: "Success!", duration: Delay.short).show()
                            }
                        })
                    }
                default:
                    break
                }
            }
            
            let noAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                (result : UIAlertAction) -> Void in
                print("cancel")
            }
            
            alertController.addAction(okAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editBWlist" {
            let des = segue.destination as! EditBWlistViewController
            if sender != nil {
                des.bw = bwlists[sender as! Int]
            }
            else {
                des.bw = BW(id: newListId, name: bwList)
            }
            des.version = version
        }
        else if segue.identifier == "editATC" {
            let des = segue.destination as! EditATCViewController
            
            if sender != nil {
                des.newListId = profiles[sender as! Int].id
            }
            else {
                des.newListId = newListId
            }
            des.profileName = profileN
            des.newATCProfile = newATCProfile
            des.version = version!
        }
        else if segue.identifier == "helpSegue" {
            let des = segue.destination as! HelpViewController
        }
        else if segue.identifier == "ATCHelpSegue" {
            let des = segue.destination as! AccessTimeControlHelpViewController
        }
    }
}

